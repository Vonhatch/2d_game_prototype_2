﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericUnit : Unit
{
    public string UnitName;
    private int aniEnemyHP;
    private int aniHP;
    private bool animating = false;

    
    //need a variable that inherits from a global object that dictates player movespeeds and whether or not to have animations that changes whether we play 
    //animations or wait for different amounts etc, where do we quickly update that variable? perhaps use an event handler?

    private Coroutine PulseCoroutine;

    public override void Initialize()
    {
        //this is base and that's really cool
        base.Initialize();

        transform.position += new Vector3(0, 0, -0.1f);
    }

    public override void OnUnitDeselected()
    {
        base.OnUnitDeselected();
        StopCoroutine(PulseCoroutine);
        transform.localScale = new Vector3(1, 1, 1);
    }

    public override void MarkAsAttacking(Unit other)
    {
        //StartCoroutine(Jerk(other));
        StartCoroutine(AnimateFight(other));
    }

    public override void MarkAsAttacked(Unit other)
    {
        UpdateHpBar();
    }

    public override void MarkAsDefending(Unit other)
    {
        StartCoroutine(Glow(new Color(1, 0, 0, 0.5f), 1));
    }
    public override void MarkAsDestroyed()
    {
    }
    public override void MarkAsHighlightedEnemy()
    {
        markedAsHighlightedEnemy = true;
        SetColor(new Color(0.8f, 0.0f, 0.0f));
    }
    public override void MarkAsItemUsed(Unit target, int itemNo)
    {
        //Debug.Log("the animation should be happeneing");
        Color col = new Color(0f,0.5f,0f);
        StartCoroutine(GlowItemUsed(col, 1f, target, itemNo));
    }

    private IEnumerator GlowItemUsed(Color color, float cooloutTime, Unit target, int itemNo)
    {
        //debug this will cause a crash if the unit isn't currently pulsing, will most likely have to fix this later.
        StopCoroutine(PulseCoroutine);
        transform.localScale = new Vector3(1, 1, 1);
        var _renderer = transform.Find("Marker").GetComponent<SpriteRenderer>();
        float startTime = Time.time;

        //I ADDED UPDATE HP BAR!!!
        //the HP bars only update after attacks because the actual value is changed after animations, fix this!!!
        //UpdateHpBar();
        CinematicUpdateHpBar(aniHP);

        while (startTime + cooloutTime > Time.time)
        {
            _renderer.color = Color.Lerp(new Color(1, 1, 1, 0), color, (startTime + cooloutTime) - Time.time);
            yield return 0;
        }
       
        _renderer.color = Color.clear;
        //the player's HP bar won't update properly i think
        EndUseConsumable(itemNo);
        UpdateHpBar();
    }

    private IEnumerator AnimateFight(Unit other)
    {
        aniEnemyHP = other.HitPoints;
        aniHP = this.HitPoints;
        foreach (fightResult res in fightResults)
        {
            if (aniEnemyHP > 0 && aniHP > 0)
            {
                if (res.attacker == 0)
                {
                    if (res.hit == 1)
                    {
                        aniEnemyHP = Mathf.Clamp(aniEnemyHP - res.damage, 0, 9999);
                    }
                    StartCoroutine(Jerk(other, res.hit));
                    yield return new WaitForSeconds(1);
                }
                else
                {
                    if (res.hit == 1)
                    {
                        aniHP = Mathf.Clamp(aniHP - res.damage, 0, 9999);
                    }
                    StartCoroutine(JerkEnemy(other, res.hit));
                    yield return new WaitForSeconds(1);
                }
            }
        }
        EndAttack(other);
        yield return 0;
    }

    public IEnumerator Jerk(Unit other, int hit)
    {
        GetComponent<SpriteRenderer>().sortingOrder = 6;
        var heading = other.transform.position - transform.position;
        var direction = heading / heading.magnitude;
        float startTime = Time.time;

        while (startTime + 0.25f > Time.time)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + (direction / 5f), ((startTime + 0.25f) - Time.time));
            yield return 0;
            //debug - this will later need to account for if any damage was done at all, crit, etc
            if (hit == 1)
            {
                other.MarkAsDefending(this);
            }
            //hackjob again but deal with it
            other.GetComponent<GenericUnit>().CinematicUpdateHpBar(aniEnemyHP);
        }
        startTime = Time.time;
        while (startTime + 0.25f > Time.time)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position - (direction / 5f), ((startTime + 0.25f) - Time.time));
            yield return 0;
        }
        //something wont work
        transform.position = Cell.transform.position + new Vector3(0, 0, -0.1f);
        GetComponent<SpriteRenderer>().sortingOrder = 4;
    }

    public IEnumerator JerkEnemy(Unit other, int hit)
    {
        other.GetComponent<SpriteRenderer>().sortingOrder = 6;
        var heading = transform.position - other.transform.position;
        var direction = heading / heading.magnitude;
        float stetTime = Time.time;

        while (stetTime + 0.25f > Time.time)
        {
            other.transform.position = Vector3.Lerp(other.transform.position, other.transform.position + (direction / 5f), ((stetTime + 0.25f) - Time.time));
            yield return 0;
            if (hit == 1)
            {
                MarkAsDefending(this);
            }
            //hackjob
            this.CinematicUpdateHpBar(aniHP);
        }
        stetTime = Time.time;
        while (stetTime + 0.25f > Time.time)
        {
            other.transform.position = Vector3.Lerp(other.transform.position, other.transform.position - (direction / 5f), ((stetTime + 0.25f) - Time.time));
            yield return 0;
        }
        other.transform.position = other.Cell.transform.position + new Vector3(0, 0, -0.1f);
        other.GetComponent<SpriteRenderer>().sortingOrder = 4;
    }


    private IEnumerator Glow(Color color, float cooloutTime)
    {
        var _renderer = transform.Find("Marker").GetComponent<SpriteRenderer>();
        float startTime = Time.time;

        //I ADDED UPDATE HP BAR!!!
        //the HP bars only update after attacks because the actual value is changed after animations, fix this!!!
        //UpdateHpBar();
        CinematicUpdateHpBar(aniHP);

        while (startTime + cooloutTime > Time.time)
        {
            _renderer.color = Color.Lerp(new Color(1, 1, 1, 0), color, (startTime + cooloutTime) - Time.time);
            yield return 0;
        }


        _renderer.color = Color.clear;
    }
    private IEnumerator Pulse(float breakTime, float delay, float scaleFactor)
    {
        var baseScale = transform.localScale;
        while (true)
        {
            float growingTime = Time.time;
            while (growingTime + delay > Time.time)
            {
                transform.localScale = Vector3.Lerp(baseScale * scaleFactor, baseScale, (growingTime + delay) - Time.time);
                yield return 0;
            }

            float shrinkingTime = Time.time;
            while (shrinkingTime + delay > Time.time)
            {
                transform.localScale = Vector3.Lerp(baseScale, baseScale * scaleFactor, (shrinkingTime + delay) - Time.time);
                yield return 0;
            }
            yield return new WaitForSeconds(breakTime);
        }
    }

    public override void MarkAsFriendly()
    {
        SetColor(new Color(0.8f, 1, 0.8f));
        if (markedAsHighlightedEnemy)
        {
            SetColor(new Color(0.8f, 0.0f, 0.0f));
        }
    }
    public override void MarkAsReachableEnemy()
    {
        SetColor(new Color(1, 0.8f, 0.8f));
        if (markedAsHighlightedEnemy)
        {
            SetColor(new Color(1f, 0.4f, 0.4f));
        }
    }
    public override void MarkAsSelected()
    {
        PulseCoroutine = StartCoroutine(Pulse(1.0f, 0.5f, 1.25f));
        SetColor(new Color(0.8f, 0.8f, 1));
    }
    public override void MarkAsFinished()
    {
        SetColor(Color.gray);
        if (markedAsHighlightedEnemy)
        {
            SetColor(new Color(0.5f, 0.0f, 0.0f));
        }
    }
    public override void UnMark()
    {
        SetColor(Color.white);
        if (markedAsHighlightedEnemy)
        {
            SetColor(new Color(0.8f, 0.0f, 0.0f));
        }
    }

    private void SetColor(Color color)
    {
        var _renderer = GetComponent<SpriteRenderer>();
        if (_renderer != null)
        {
            _renderer.color = color;
        }
    }

    private void UpdateHpBar()
    {
        if (GetComponentInChildren<Image>() != null)
        {
            GetComponentInChildren<Image>().transform.localScale = new Vector3((float)((float)HitPoints / (float)TotalHitPoints), 1, 1);
            GetComponentInChildren<Image>().color = Color.Lerp(Color.red, Color.green,
                (float)((float)HitPoints / (float)TotalHitPoints));
        }
    }

    public void CinematicUpdateHpBar(int hp)
    {
        if (GetComponentInChildren<Image>() != null)
        {
            GetComponentInChildren<Image>().transform.localScale = new Vector3((float)((float)hp / (float)TotalHitPoints), 1, 1);
            GetComponentInChildren<Image>().color = Color.Lerp(Color.red, Color.green,
                (float)((float)hp / (float)TotalHitPoints));
        }
    }
}

