﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
public class MySquare : Square
{

    private bool markedAsReachable = false;
    private bool markedAsPath = false;
    private bool markedAsHighlighted = false;
    private bool markedAsEnemyReachable = false;
    public bool markedAsAttackable = false;

    public void Start()
    {
        transform.Find("Highlighter").GetComponent<SpriteRenderer>().sortingOrder = 3;
    }

    public override Vector3 GetCellDimensions()
    {
        var ret = GetComponent<SpriteRenderer>().bounds.size;
        return ret*0.98f;
    }

    public override void MarkAsReachable()
    {
        markedAsReachable = false;
        markedAsPath = false;
        markedAsHighlighted = false;

        SetColor(new Color(1,0.92f,0.16f,0.5f));

        markedAsReachable = true;
    }
    public override void MarkAsPath()
    {
        markedAsReachable = false;
        markedAsPath = false;
        markedAsHighlighted = false;

        SetColor(new Color(0,1,0,0.5f));

        markedAsPath = true;
    }
    public override void MarkAsHighlighted()
    {
        markedAsReachable = false;
        markedAsPath = false;
        markedAsHighlighted = false;

        SetColor(new Color(0.8f,0.8f,0.8f,0.5f));

        markedAsHighlighted = true;
    }
    public override void MarkAsEnemyReachable()
    { 
        markedAsEnemyReachable = true;

        DrawCell();
    }
    public override void MarkAsAttackable()
    {
        
        //markedAsReachable = false;
        markedAsPath = false;
        markedAsHighlighted = false;
        //markedAsAttackable = false;


        if (markedAsReachable)
        {
            return;
        }
        else
        {
            //Debug.Log("this ran");
            SetColor(new Color(1.0f, 0.0f, 0.0f, 0.5f));
        }

        markedAsAttackable = true;
       // DrawCell();
    }
    public override void UnMarkAsEnemyReachable()
    {
        markedAsEnemyReachable = false;

        DrawCell();
    }

    public override void UnMark()
    {
        markedAsReachable = false;
        markedAsPath = false;
        markedAsHighlighted = false;
        markedAsAttackable= false;

        DrawCell();
        SetColor(new Color(1,1,1,0));
    }

    public override void Dehilight()
    {
        markedAsReachable = false;
        markedAsPath = false;
        markedAsHighlighted = false;
        //markedAsAttackable = false;

        DrawCell();
        if (!markedAsAttackable)
        {
            //Debug.Log("erased nonred square");
            SetColor(new Color(1, 1, 1, 0));
        }
        else if (markedAsAttackable){
            SetColor(new Color(1.0f, 0.0f, 0.0f, 0.5f));
        }
    }

    public void DrawCell()
    {
        Color col;
        col = new Color(1f, 1f, 1f, 0f);
        if (markedAsEnemyReachable)
        {
            col -= new Color(0.2f, 1.0f, 1.0f, -0.5f);
        }
        
        SetTileColor(col);

        return;
        if (markedAsAttackable && !markedAsReachable)
        {
            SetColor(new Color(0.2f, 1.0f, 1.0f, -0.5f));
        }
    }

    private void SetTileColor(Color color)
    {
            var newhighlighter = transform.Find("FullHighlighter");
            if (newhighlighter != null)
            {
                var newspriteRenderer = newhighlighter.GetComponent<SpriteRenderer>();
                if (newspriteRenderer != null)
                {
                    newspriteRenderer.color = color;
                }
            }
    }

    private void SetColor(Color color)
    {
        var highlighter = transform.Find("Highlighter");
        var spriteRenderer = highlighter.GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.color = color;
        }
    }
}
