﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class NextGuiController : MonoBehaviour
{
    public CellGrid CellGrid;

    public Button NextTurnButton;

    public GameObject InfoPanel;
    public GameObject BattlePanel;
    public GameObject GameOverPanel;
    public GameObject ConfirmationPanel;
    public GameObject PlayerMenu;
    public GameObject LevelPanel;
    public GameObject Inventory;
    public GameObject ItemPanel;
    public Text EquippedTag;

    public AttackBtn AttackButton;
    public ItemBtn ItemButton;

    public Canvas Canvas;

    //this is lazy, fix later
    public bool levelPanelDisplaying = false;
    private bool levelPanelDisplayFinishedAnimation = true;

    //this is again ugly code debug
    private int startingWeapon = 9999;

    private GameObject _infoPanel;
    private GameObject _battlePanel;
    private GameObject _inventoryPanel;
    private GameObject _gameOverPanel;
    private GameObject _confirmationPanel;
    private GameObject _playerMenu;
    private GameObject _levelPanel;
    private GameObject _itemPanel;

    private Coroutine levelUpAnimation;

    private GenericUnit levellingUnit;
    private LevelUpResult result;

    //a string needed for a check to overcome incorrectly functioning unit selection
    private string clickedUnit = "boogawooga";

    public GenericUnit selectee;

    private bool isGameOver;
    private bool unitSelected = false;

    public Unit intendedAttacker;
    public Unit intendedDefender;

    void Awake()
    {
        CellGrid.TurnEnded += OnTurnEnded;
        CellGrid.GameEnded += OnGameEnded;
        CellGrid.UnitAdded += OnUnitAdded;
    }

    private void OnTargetChosen(object sender, EventArgs e)
    {
        //Debug.Log("this is workg!!!111");
    }

    private void OnTurnEnded(object sender, EventArgs e)
    {
        NextTurnButton.interactable = ((sender as CellGrid).CurrentPlayer is HumanPlayer);
        Destroy(_infoPanel);
        Destroy(_battlePanel);
        Destroy(_playerMenu);
        unitSelected = false;
    }
    private void OnGameEnded(object sender, EventArgs e)
    {
        isGameOver = true;
        _gameOverPanel = Instantiate(GameOverPanel);
        _gameOverPanel.transform.Find("InfoText").GetComponent<Text>().text = "Player " + ((sender as CellGrid).CurrentPlayerNumber + 1) + "\nwins!";
        
        _gameOverPanel.transform.Find("DismissButton").GetComponent<Button>().onClick.AddListener(DismissPanel);
 
        _gameOverPanel.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);

    }

    private void OnUnitAttacked(object sender, AttackEventArgs e)
    {
        Destroy(_battlePanel);
        Destroy(_infoPanel);
        unitSelected = false;

        if (!(CellGrid.CurrentPlayer is HumanPlayer)) return;

        OnUnitDehighlighted(sender, e);

        if ((sender as Unit).HitPoints <= 0) return;

        OnUnitHighlighted(sender, e);
    }
    private void OnUnitDestroyed(object sender, AttackEventArgs e)
    {
        Destroy(_infoPanel);
        Destroy(_battlePanel);
        unitSelected = false;
    }

    private void OnUnitSelected(object sender, EventArgs e)
    {
        var unit = sender as GenericUnit;
        selectee = sender as GenericUnit;
        
        if (!(unit.UnitState is UnitStateMarkedAsFinished))
        {
            unitSelected = true;
            if (!selectee.isMenuing)
            {
                _playerMenu = Instantiate(PlayerMenu);
                _playerMenu.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);
            }
        }

    }

    private void OnUnitClicked(object sender, EventArgs e)
    {
        if (isGameOver)
            return;
        Destroy(_infoPanel);
        var unit = sender as GenericUnit;

        DisplayInfoPanel(unit);

        //code here to bring up the UI for each unit, and close all the extra menu stuff that may have been opened
    }

    private void OnUnitRightClicked(object sender, EventArgs e)
    {
        var unit = sender as GenericUnit;

        if (unit == selectee)
        {
            //print("unit is selectee");
            //selectee.ResetPosition();
            //selectee.isAttacking = false;
            Destroy(_confirmationPanel);
            Destroy(_infoPanel);
            //Destroy(_playerMenu);
        }
    }

    private void OnUnitDeselected(object sender, EventArgs e)
    {
        unitSelected = false;
        //code here to close whatever unit stuff was previously avaialable?
        Destroy(_playerMenu);
        Destroy(_infoPanel);
    }

    private void OnUnitDehighlighted(object sender, EventArgs e)
    {
        if (isGameOver)
            return;

        Destroy(_infoPanel);
        Destroy(_battlePanel);
    }
    private void OnUnitHighlighted(object sender, EventArgs e)
    {
        if (isGameOver)
            return;

        var unit = sender as GenericUnit;

        DisplayBattlePanel(unit);

            //this is the old way of doing it, however because the attack code and the UI code both use the same fightresults list
            //there can be cases of overlap where the collection is modified whilst in use, it should be fixable by being smart but 
            //I'm not so this stays here unti I figure out how to implement it correctly
            /*
            selectee.GetBattleResults(unit);

            int HP1 = selectee.HitPoints;
            int HP2 = unit.HitPoints;
            int noAttacks1 = 0;
            int noAttacks2 = 0;
            int ATK1 = 0;
            int ATK2 = 0;

            foreach (fightResult res in selectee.fightResults)
            {
                if (res.attacker == 1)
                {
                    HP1 = (int)Mathf.Clamp(HP1 - res.damage, 0f, 9999f);
                    noAttacks2++;
                    ATK2 = res.damage;
                }
                else
                {
                    HP2 = (int)Mathf.Clamp(HP2 - res.damage, 0f, 9999f);
                    noAttacks1++;
                    ATK1 = res.damage;
                }
            }*/

    }

    private void DisplayBattlePanel(GenericUnit unit)
    {
        if (isGameOver)
            return;

        Destroy(_battlePanel);
        DisplayInfoPanel(unit);


        //This will always assume the player can hit the enemy irrespective of if they can even reach them this turn, otherwise works fine.
        //This uses getInfoBattle results but I don't like this and I think we can go back to the old way as soon as we fix the info panel glitch
        if (unitSelected && (unit.PlayerNumber != selectee.PlayerNumber) && !selectee.turnEnded)
        {
            _battlePanel = Instantiate(BattlePanel);

            _battlePanel.transform.Find("Name1").GetComponent<Text>().text = selectee.UnitName;
            _battlePanel.transform.Find("Name2").GetComponent<Text>().text = unit.UnitName;

            //sdsdsdsdsd
            selectee.GetInfoBattleResults(unit);

            int HP1 = selectee.HitPoints;
            int HP2 = unit.HitPoints;
            int noAttacks1 = 0;
            int noAttacks2 = 0;
            int ATK1 = 0;
            int ATK2 = 0;

            foreach (fightResult res in selectee.fightResults2)
            {
                if (res.attacker == 1)
                {
                    HP1 = (int)Mathf.Clamp(HP1 - res.damage, 0f, 9999f);
                    noAttacks2++;
                    ATK2 = res.damage;
                }
                else
                {
                    HP2 = (int)Mathf.Clamp(HP2 - res.damage, 0f, 9999f);
                    noAttacks1++;
                    ATK1 = res.damage;
                }
            }

            _battlePanel.transform.Find("HP2").Find("Text").GetComponent<Text>().text = unit.HitPoints.ToString() + "->" + HP2.ToString();
            _battlePanel.transform.Find("HP1").Find("Text").GetComponent<Text>().text = selectee.HitPoints.ToString() + "->" + HP1.ToString();

            if (noAttacks1 > 0)
            {
                _battlePanel.transform.Find("ATK1").Find("Text").GetComponent<Text>().text = ATK1.ToString() + "x" + noAttacks1.ToString();
                _battlePanel.transform.Find("Hit1").Find("Text").GetComponent<Text>().text = Mathf.Clamp(selectee.hitChance(unit), 0f, 100f).ToString();
            }
            else
            {
                _battlePanel.transform.Find("ATK1").Find("Text").GetComponent<Text>().text = "-";
                _battlePanel.transform.Find("Hit1").Find("Text").GetComponent<Text>().text = "-";
            }

            if (noAttacks2 > 0)
            {
                _battlePanel.transform.Find("ATK2").Find("Text").GetComponent<Text>().text = ATK2.ToString() + "x" + noAttacks2.ToString();
                _battlePanel.transform.Find("Hit2").Find("Text").GetComponent<Text>().text = Mathf.Clamp(unit.hitChance(selectee), 0f, 100f).ToString();
            }
            else
            {
                _battlePanel.transform.Find("ATK2").Find("Text").GetComponent<Text>().text = "-";
                _battlePanel.transform.Find("Hit2").Find("Text").GetComponent<Text>().text = "-";
            }

            _battlePanel.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);
        }
    }

    private void OnUnitAdded(object sender, UnitCreatedEventArgs e)
    {
        RegisterUnit(e.unit);

        //lazy risky code that prevents a null reference exception if the player ends their turn without ever selecting a unit.
        //FIX THIS LATER!!!
        var unit = sender as GenericUnit;
        selectee = unit;
    }

    public virtual void OnUnitChosenForAttack(object sender, BattleEventArgs e)
    {
        var unit = sender as GenericUnit;
        if (unit.isMenuing)
        {
            return;
        }

        //unit.isAttacking = true;
        unit.isMenuing = true;

        intendedAttacker = e.Attacker;
        intendedDefender = e.Defender;

        foreach (Item item in intendedAttacker.Inventory)
        {
            if (item is Weapon && item.isEquiped)
            {
                startingWeapon = intendedAttacker.Inventory.IndexOf(item);
            }
        }

        _confirmationPanel = Instantiate(ConfirmationPanel);



        _confirmationPanel.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);

        int counter = 0;
        int poscounter = 0;
        foreach (Item item in unit.Inventory)
        {
            if(item is Weapon)
            {
                Weapon wep = (Weapon)item;
                AttackBtn btn;
                btn = Instantiate(AttackButton);
                btn.btnNo = counter;

                btn.transform.Find("Text").GetComponent<Text>().text = item.itemName;//"Attack " + counter.ToString();

                Vector3 temp = _confirmationPanel.transform.position;
                temp += new Vector3(0f - 1885f, (float)poscounter * -65f - 1800f, 0f);

                //Vector3 temp = new Vector3(0f - 99f, (float)poscounter * -75f, 0f);
                btn.GetComponent<RectTransform>().SetPositionAndRotation(btn.GetComponent<RectTransform>().position + temp, btn.GetComponent<RectTransform>().rotation);
                btn.GetComponent<RectTransform>().SetParent(_confirmationPanel.GetComponent<RectTransform>(), false);

                if (!unit.isWeaponEquippable(wep) || !unit.IsUnitAttackable(intendedDefender, unit.Cell, wep))
                {
                    btn.GetComponent<Button>().interactable = false;
                }

                if (item.isEquiped)
                {
                    btn.transform.Find("Equippedtext").GetComponent<Text>().text = "EQ";
                }
                else
                {
                    btn.transform.Find("Equippedtext").GetComponent<Text>().text = "";
                }

                poscounter++;
                if (item.maxCharges != 9999)
                {
                    btn.transform.Find("DurabilityText").GetComponent<Text>().text = item.currentCharges.ToString() + "/" + item.maxCharges.ToString() ;
                }

            }
            counter++;
        }
    }

    public void OpenInventory()
    {

        //unit.isAttacking = true;
        selectee.isMenuing = true;

        Destroy(_inventoryPanel);
        _inventoryPanel = Instantiate(Inventory);



        _inventoryPanel.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);

        int counter = 0;
        foreach (Item item in selectee.Inventory)
        {

                ItemBtn btn;
                btn = Instantiate(ItemButton);
                btn.btnNo = counter;

                btn.transform.Find("Text").GetComponent<Text>().text = item.itemName;//"Attack " + counter.ToString();

                Vector3 temp = new Vector3(10f, (float)counter * -65f - 25f, 0f);
                btn.GetComponent<RectTransform>().SetPositionAndRotation(btn.GetComponent<RectTransform>().position + temp, btn.GetComponent<RectTransform>().rotation);
                btn.GetComponent<RectTransform>().SetParent(_inventoryPanel.GetComponent<RectTransform>(), false);

            if (item.isEquiped)
            {
                btn.transform.Find("Equippedtext").GetComponent<Text>().text = "EQ";
            }
            else
            {
                btn.transform.Find("Equippedtext").GetComponent<Text>().text = "";
            }

                counter++;

            if (item.maxCharges != 9999)
            {
                btn.transform.Find("DurabilityText").GetComponent<Text>().text = item.currentCharges.ToString() + "/" + item.maxCharges.ToString();
            }
        }
    }

    public void ItemHighlighted(int itemNo)
    {
        //debug I don't like this implementation but it stays for now
        selectee.EquipItem(itemNo);
        //debug this cast will cause errors once i make more complicated units, this code will then need to be fixed up
        DisplayBattlePanel((GenericUnit)intendedDefender);
    }

    public void ItemSelected(int itemNo)
    {
        //debug yucky code
        if(selectee.Inventory[itemNo] is ConsumableItem)
        {
            selectee.ItemSelected(itemNo);
        }
        else
        {
            selectee.ItemSelected(itemNo);
            OpenInventory();
        }
        //selectee.ItemSelected(itemNo);
    }

    public virtual void OnUnitLevelled(object sender, LevelEventArgs e)
    {
        //print("this ran");
        levelPanelDisplaying = true;
        levelPanelDisplayFinishedAnimation = false;
        levellingUnit = sender as GenericUnit;
        result = e.result;

        levelUpAnimation = StartCoroutine(displayLevelPanel());
    }

    private IEnumerator displayLevelPanel()
    {
        //print("this ran");
        _levelPanel = Instantiate(LevelPanel);


        _levelPanel.transform.Find("Name").GetComponent<Text>().text = levellingUnit.UnitName;
        _levelPanel.transform.Find("Level").GetComponent<Text>().text = "Level: " + levellingUnit.level.ToString();
        _levelPanel.transform.Find("HitPoints").Find("Text").GetComponent<Text>().text = levellingUnit.TotalHitPoints.ToString();
        _levelPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().text = levellingUnit.AttackFactor.ToString();
        _levelPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().text = levellingUnit.DefenceFactor.ToString();
        _levelPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().text = levellingUnit.TechnicalFactor.ToString();
        _levelPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().text = levellingUnit.TechDefenceFactor.ToString();
        _levelPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().text = levellingUnit.SkillFactor.ToString();
        _levelPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().text = levellingUnit.SpeedFactor.ToString();
        _levelPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().text = levellingUnit.LuckFactor.ToString();

        _levelPanel.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);
        //yield return new WaitForSeconds(3);
        //print("waited");
        _levelPanel.transform.Find("Name").GetComponent<Text>().text = levellingUnit.UnitName;

        yield return new WaitForSeconds(1f);
        _levelPanel.transform.Find("Level").GetComponent<Text>().text = "Level: " + levellingUnit.level.ToString() + " + 1";
        _levelPanel.transform.Find("Level").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);

        if (result.HitPoints > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("HitPoints").Find("Text").GetComponent<Text>().text = levellingUnit.TotalHitPoints.ToString() + " + " + result.HitPoints;
            _levelPanel.transform.Find("HitPoints").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }
        if (result.AttackFactor > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().text = levellingUnit.AttackFactor.ToString() + " + " + result.AttackFactor;
            _levelPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }
        if (result.DefenceFactor > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().text = levellingUnit.DefenceFactor.ToString() + " + " + result.DefenceFactor;
            _levelPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }
        if (result.TechnicalFactor > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().text = levellingUnit.TechnicalFactor.ToString() + " + " + result.TechnicalFactor;
            _levelPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }
        if (result.TechDefenceFactor > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().text = levellingUnit.TechDefenceFactor.ToString() + " + " + result.TechDefenceFactor;
            _levelPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }
        if (result.SkillFactor > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().text = levellingUnit.SkillFactor.ToString() + " + " + result.SkillFactor;
            _levelPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }
        if (result.SpeedFactor > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().text = levellingUnit.SpeedFactor.ToString() + " + " + result.SpeedFactor;
            _levelPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }
        if (result.LuckFactor > 0)
        {
            yield return new WaitForSeconds(0.5f);
            _levelPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().text = levellingUnit.LuckFactor.ToString() + " + " + result.LuckFactor;
            _levelPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
        }


        levelPanelDisplayFinishedAnimation = true;
        yield break;
    }

    public virtual void CloseUnitLevelledPanel()
    {
        //print("thisran");
        if (levelPanelDisplayFinishedAnimation)
        {
            levelPanelDisplaying = false;
            Destroy(_levelPanel);
        }
        else
        {
            StopCoroutine(levelUpAnimation);
            _levelPanel.transform.Find("Name").GetComponent<Text>().text = levellingUnit.UnitName;
            _levelPanel.transform.Find("Level").GetComponent<Text>().text = "Level: " + levellingUnit.level.ToString() + " + 1";
            _levelPanel.transform.Find("Level").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);

            if (result.HitPoints > 0)
            {
                _levelPanel.transform.Find("HitPoints").Find("Text").GetComponent<Text>().text = levellingUnit.TotalHitPoints.ToString() + " + " + result.HitPoints;
                _levelPanel.transform.Find("HitPoints").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }
            if (result.AttackFactor > 0)
            {
                _levelPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().text = levellingUnit.AttackFactor.ToString() + " + " + result.AttackFactor;
                _levelPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }
            if (result.DefenceFactor > 0)
            {
                _levelPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().text = levellingUnit.DefenceFactor.ToString() + " + " + result.DefenceFactor;
                _levelPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }
            if (result.TechnicalFactor > 0)
            {
                _levelPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().text = levellingUnit.TechnicalFactor.ToString() + " + " + result.TechnicalFactor;
                _levelPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }
            if (result.TechDefenceFactor > 0)
            {
                _levelPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().text = levellingUnit.TechDefenceFactor.ToString() + " + " + result.TechDefenceFactor;
                _levelPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }
            if (result.SkillFactor > 0)
            {
                _levelPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().text = levellingUnit.SkillFactor.ToString() + " + " + result.SkillFactor;
                _levelPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }
            if (result.SpeedFactor > 0)
            {
                _levelPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().text = levellingUnit.SpeedFactor.ToString() + " + " + result.SpeedFactor;
                _levelPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }
            if (result.LuckFactor > 0)
            {
                _levelPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().text = levellingUnit.LuckFactor.ToString() + " + " + result.LuckFactor;
                _levelPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            }

            
            levelPanelDisplayFinishedAnimation = true;
        }
        
    }

    public void Attack(int itemNo)
    {
        //in theory this should be unneccessary because mousing over the weapon to use will cause the item to already be equipped but better safe than sorry
        intendedAttacker.EquipItem(itemNo);

        intendedAttacker.Attack(intendedDefender);
        intendedAttacker.isMenuing = false;
        Destroy(_confirmationPanel);
        Destroy(_playerMenu);
        DestroyItemPanel();
        unitSelected = false;
        //kind of a yucky fix, needs to be worked on, but ensures if the player attacks without moving the gridstate is changed.
        //selectee.EndPlayerUnitTurn();
    }

    public void CancelAttack()
    {
        if (startingWeapon != 9999)
        {
            intendedAttacker.EquipItem(startingWeapon);
        }
        else
        {
            foreach(Item item in intendedAttacker.Inventory)
            {
                if(item is Weapon && item.isEquiped == true)
                {
                    intendedAttacker.UnequipItem(intendedAttacker.Inventory.IndexOf(item));
                }
            }
            //intendedAttacker.UnequipItem()
        }

        intendedAttacker.isAttacking = false;
        intendedAttacker.isMenuing = false;

        Destroy(_confirmationPanel);
        Destroy(_itemPanel);
        //more really bad code
        //intendedAttacker.OnUnitSelected();
    }

    public void ShowItemPanel(int number)
    {
        Item item = selectee.Inventory[number];
        Destroy(_itemPanel);
        _itemPanel = Instantiate(ItemPanel);

        _itemPanel.transform.Find("ItemName").GetComponent<Text>().text = item.itemName;
        if (item is Weapon)
        {
            Weapon wep = (Weapon)item;
            _itemPanel.transform.Find("Attack").GetComponent<Text>().text = wep.attackModifier.ToString();
            

            if (wep.attackRangeMax == wep.attackRangeMin)
            {
                _itemPanel.transform.Find("Range").GetComponent<Text>().text = wep.attackRangeMin.ToString();
            }
            else
            {
                _itemPanel.transform.Find("Range").GetComponent<Text>().text = wep.attackRangeMin.ToString() + "-" + wep.attackRangeMax.ToString();
            }
        }
        else
        {
            _itemPanel.transform.Find("Attack").GetComponent<Text>().text = "";
            _itemPanel.transform.Find("AttackTitle").GetComponent<Text>().text = "";
            _itemPanel.transform.Find("RangeTitle").GetComponent<Text>().text = "";
            _itemPanel.transform.Find("Range").GetComponent<Text>().text = "";

        }

        _itemPanel.transform.Find("Durability").GetComponent<Text>().text = "";
        if (item.maxCharges != 9999)
        {

            _itemPanel.transform.Find("Durability").GetComponent<Text>().text = item.currentCharges.ToString() + "/" + item.maxCharges.ToString();
        }
        _itemPanel.transform.Find("Description").GetComponent<Text>().text = item.description;

        _itemPanel.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);
    }

    public void DestroyItemPanel()
    {
        Destroy(_itemPanel);
    }

    public void CloseInventory()
    {    
        selectee.isMenuing = false;
        //Debug.Log("close inventory ran");
        //_inventoryPanel.transform.SetPositionAndRotation(_inventoryPanel.transform.position*99, _inventoryPanel.transform.rotation);
        Destroy(_inventoryPanel);
        DestroyItemPanel();
        //GameObject todie = _inventoryPanel;
        //Destroy(todie);
    }

    public void EndPlayerUnitTurn()
    {
        Destroy(_playerMenu);

        CloseInventory();
        //gonna cause an exception error if the player ends their turn without ever having selected a unit.
        selectee.EndPlayerUnitTurn();
        selectee.OnUnitDeselected();
        unitSelected = false;
    }

    public void OnUnitUsedConsumable(object sender, EventArgs e)
    {
        //Destroy(_inventoryPanel.gameObject);

        CloseInventory();
        //EndPlayerUnitTurn();
    }

    private void RegisterUnit(Transform unit)
    {
        unit.GetComponent<Unit>().UnitHighlighted += OnUnitHighlighted;
        unit.GetComponent<Unit>().UnitDehighlighted += OnUnitDehighlighted;
        unit.GetComponent<Unit>().UnitDestroyed += OnUnitDestroyed;
        unit.GetComponent<Unit>().UnitAttacked += OnUnitAttacked;
        unit.GetComponent<Unit>().UnitSelected += OnUnitSelected;
        unit.GetComponent<Unit>().UnitDeselected += OnUnitDeselected;
        unit.GetComponent<Unit>().UnitUsedConsumable += OnUnitUsedConsumable;


        unit.GetComponent<Unit>().UnitClicked += OnUnitClicked;

        unit.GetComponent<Unit>().UnitRightClicked += OnUnitRightClicked;

        unit.GetComponent<Unit>().UnitChosenForAttack += OnUnitChosenForAttack;

        unit.GetComponent<Unit>().UnitLevelled += OnUnitLevelled;
    }
    public void DismissPanel()
    {
        Destroy(_gameOverPanel);
    }
    public void RestartLevel()
    {
        isGameOver = false;
        Application.LoadLevel(Application.loadedLevel);
    }


    private void DisplayInfoPanel(GenericUnit unit)
    {
        Destroy(_infoPanel);
        _infoPanel = Instantiate(InfoPanel);

        float hpScale = (float)((float)(unit).HitPoints / (float)(unit).TotalHitPoints);

        _infoPanel.transform.Find("Name").GetComponent<Text>().text = unit.UnitName;
        _infoPanel.transform.Find("Level").GetComponent<Text>().text = "Level: " + unit.level.ToString();
        _infoPanel.transform.Find("Movement").Find("Text").GetComponent<Text>().text = unit.TotalMovementPoints.ToString();
        //more hardcoded player number stuff
        if (unit.PlayerNumber == 0)
        {
            _infoPanel.transform.Find("ExperiencePoints").GetComponent<Text>().text = "EXP: " + unit.experiencePoints;
        }
        else
        {
            _infoPanel.transform.Find("ExperiencePoints").GetComponent<Text>().text = "";
        }
        _infoPanel.transform.Find("HitPoints").Find("Text").GetComponent<Text>().text = unit.HitPoints.ToString() + "/" + unit.TotalHitPoints.ToString();

        if (unit.ModdedAttackFactor > unit.AttackFactor)
        {
            _infoPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            _infoPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().text = unit.ModdedAttackFactor.ToString() + " (+" + (unit.ModdedAttackFactor - unit.AttackFactor) + ")";
        }
        else if (unit.ModdedAttackFactor < unit.AttackFactor)
        {
            _infoPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().color = new Color(0.5f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().text = unit.ModdedAttackFactor.ToString() + " (" + (unit.ModdedAttackFactor - unit.AttackFactor) + ")";
        }
        else
        {
            _infoPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().color = new Color(0.0f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().text = unit.ModdedAttackFactor.ToString();
        }

        if (unit.ModdedDefenceFactor > unit.DefenceFactor)
        {
            _infoPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            _infoPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().text = unit.ModdedDefenceFactor.ToString() + " (+" + (unit.ModdedDefenceFactor - unit.DefenceFactor) + ")";

        }
        else if (unit.ModdedDefenceFactor < unit.DefenceFactor)
        {
            _infoPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().color = new Color(0.5f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().text = unit.ModdedDefenceFactor.ToString() + " (" + (unit.ModdedDefenceFactor - unit.DefenceFactor) + ")";
        }
        else
        {
            _infoPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().color = new Color(0.0f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Defense").Find("Text").GetComponent<Text>().text = unit.ModdedDefenceFactor.ToString();
        }

        if (unit.ModdedTechnicalFactor > unit.TechnicalFactor)
        {
            _infoPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            _infoPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().text = unit.ModdedTechnicalFactor.ToString() + " (+" + (unit.ModdedTechnicalFactor - unit.TechnicalFactor) + ")";
        }
        else if (unit.ModdedTechnicalFactor < unit.TechnicalFactor)
        {
            _infoPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().color = new Color(0.5f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().text = unit.ModdedTechnicalFactor.ToString() + " (" + (unit.ModdedTechnicalFactor - unit.TechnicalFactor) + ")";
        }
        else
        {
            _infoPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().color = new Color(0.0f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Tech").Find("Text").GetComponent<Text>().text = unit.ModdedTechnicalFactor.ToString();
        }

        if (unit.ModdedTechDefenceFactor > unit.TechDefenceFactor)
        {
            _infoPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            _infoPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().text = unit.ModdedTechDefenceFactor.ToString() + " (+" + (unit.ModdedTechDefenceFactor - unit.TechDefenceFactor) + ")";
        }
        else if (unit.ModdedTechDefenceFactor < unit.TechDefenceFactor)
        {
            _infoPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().color = new Color(0.5f, 0.0f, 0.0f);
            _infoPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().text = unit.ModdedTechDefenceFactor.ToString() + " (" + (unit.ModdedTechDefenceFactor - unit.TechDefenceFactor) + ")";
        }
        else
        {
            _infoPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().color = new Color(0.0f, 0.0f, 0.0f);
            _infoPanel.transform.Find("TechDef").Find("Text").GetComponent<Text>().text = unit.ModdedTechDefenceFactor.ToString();
        }

        if (unit.ModdedSkillFactor > unit.SkillFactor)
        {
            _infoPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            _infoPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().text = unit.ModdedSkillFactor.ToString() + " (+" + (unit.ModdedSkillFactor - unit.SkillFactor) + ")";
        }
        else if (unit.ModdedSkillFactor < unit.SkillFactor)
        {
            _infoPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().color = new Color(0.5f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().text = unit.ModdedSkillFactor.ToString() + " (" + (unit.ModdedSkillFactor - unit.SkillFactor) + ")";
        }
        else
        {
            _infoPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().color = new Color(0.0f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Skill").Find("Text").GetComponent<Text>().text = unit.ModdedSkillFactor.ToString();
        }

        if (unit.ModdedSpeedFactor > unit.SpeedFactor)
        {
            _infoPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            _infoPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().text = unit.ModdedSpeedFactor.ToString() + " (+" + (unit.ModdedSpeedFactor - unit.SpeedFactor) + ")";
        }
        else if (unit.ModdedSpeedFactor < unit.SpeedFactor)
        {
            _infoPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().color = new Color(0.5f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().text = unit.ModdedSpeedFactor.ToString() + " (" + (unit.ModdedSpeedFactor - unit.SpeedFactor) + ")";
        }
        else
        {
            _infoPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().color = new Color(0.0f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Speed").Find("Text").GetComponent<Text>().text = unit.ModdedSpeedFactor.ToString();
        }

        if (unit.ModdedLuckFactor > unit.LuckFactor)
        {
            _infoPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().color = new Color(0f, 0.4f, 0.0f);
            _infoPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().text = unit.ModdedLuckFactor.ToString() + " (+" + (unit.ModdedLuckFactor - unit.LuckFactor) + ")";
        }
        else if (unit.ModdedLuckFactor < unit.LuckFactor)
        {
            _infoPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().color = new Color(0.5f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().text = unit.ModdedLuckFactor.ToString() + " (" + (unit.ModdedLuckFactor - unit.LuckFactor) + ")";
        }
        else
        {
            _infoPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().color = new Color(0.0f, 0.0f, 0.0f);
            _infoPanel.transform.Find("Luck").Find("Text").GetComponent<Text>().text = unit.ModdedLuckFactor.ToString();
        }


        //_infoPanel.transform.Find("Attack").Find("Text").GetComponent<Text>().text = unit.ModdedAttackFactor.ToString();
        
        
        
        
        
        
        _infoPanel.transform.Find("Atk").Find("Text").GetComponent<Text>().text = unit.totalAttackValue.ToString();

        _infoPanel.GetComponent<RectTransform>().SetParent(Canvas.GetComponent<RectTransform>(), false);
    }
}

