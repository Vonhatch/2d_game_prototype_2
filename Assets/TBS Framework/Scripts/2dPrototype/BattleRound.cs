﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleRound : MonoBehaviour {

    private int attacker; //0 is unit that initiated, 1 is attackee
    private int damage; //damage taken
    private int hit; // 0 miss, 1 hit, this is ints because i may expans later and am lazy now

    public BattleRound(int atk, int dam, int hitt)
    {
        attacker = atk;
        damage = dam;
        hit = hitt;
    }

    public BattleRound getBattleRound()
    {
        return this;
    }  

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
