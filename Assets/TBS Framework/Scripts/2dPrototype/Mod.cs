﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mod {

    public string modName = "default";

    public int HitPoints = 0;

    public int totalAttackValue = 0;
    public int hitValue = 0;

    public int AttackFactor = 0;
    public int DefenceFactor = 0;
    public int SkillFactor = 0;
    public int SpeedFactor = 0;
    public int TechnicalFactor = 0;
    public int TechDefenceFactor = 0;
    public int LuckFactor = 0;
}
