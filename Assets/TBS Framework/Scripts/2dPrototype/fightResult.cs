﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct fightResult{

        public int attacker;
        public int damage;
        public int hit;
        public fightResult(int a, int b, int c)
        {
            attacker = a;
            damage = b;
            hit = c;
        }

        public override string ToString()
        {
            return ("attacker: " + attacker.ToString() + " damage: " + damage.ToString() + " hit: " + hit.ToString());
        }

}
