﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item {

    public int attackRangeMin;
    public int attackRangeMax;

    public int attackType = 0;
    public int weaponType;

    public int attackModifier = 0;
    public int hitModifier = 0;
    public int critModifier = 0;

}
