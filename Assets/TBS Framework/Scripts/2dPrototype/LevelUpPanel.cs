﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpPanel : MonoBehaviour {

    public virtual void ClosePanel()
    {
        GameObject GUICon = GameObject.Find("GUIController");
           
        if(GUICon != null)
        {
            GUICon.SendMessage("CloseUnitLevelledPanel");
        }
    }
}
