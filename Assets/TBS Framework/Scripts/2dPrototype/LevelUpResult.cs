﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpResult {

    public int HitPoints = 0;

    public int AttackFactor = 0;
    public int DefenceFactor = 0;
    public int SkillFactor = 0;
    public int SpeedFactor = 0;
    public int TechnicalFactor = 0;
    public int TechDefenceFactor = 0;
    public int LuckFactor = 0;

    public LevelUpResult()
    {
    }


    public LevelUpResult(int atk, int def, int skl, int spd, int tec, int tdf, int lck)
    {
        AttackFactor = atk;
        DefenceFactor = def;
        SkillFactor = skl;
        SpeedFactor = spd;
        TechnicalFactor = tec;
        TechDefenceFactor = tdf;
        LuckFactor = lck;
    }

    
    public override string ToString()
    {
        return ("HP: " + HitPoints.ToString() + " atk: " + AttackFactor.ToString() + " def: " + DefenceFactor.ToString() + " skl: " + SkillFactor.ToString() + " spd: " + SpeedFactor.ToString()
            + " tec: " + TechnicalFactor.ToString() + " tdf: " + TechDefenceFactor.ToString() + " lck: " + LuckFactor.ToString());
    }
}
