﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ItemBtn : MonoBehaviour, IPointerEnterHandler
{
    public int btnNo = 0;

    //public UnityEvent atk1Pressed;
    public void OnPointerEnter(PointerEventData eventData)
    {
        // Do something.
        //Debug.Log("<color=red>Event:</color> Completed mouse highlight.");
        GameObject.Find("GUIController").GetComponent<NextGuiController>().ShowItemPanel(btnNo);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        // Do something.
        //Debug.Log("<color=red>Event:</color> Completed mouse highlight.");
        GameObject.Find("GUIController").GetComponent<NextGuiController>().DestroyItemPanel();
    }
    public void ButtonPressed()
    {
        //atk1Pressed.Invoke();
        //this is bad code and needs to be reworked at some point
        GameObject.Find("GUIController").GetComponent<NextGuiController>().ItemSelected(btnNo);
        //GameObject.Find("GUIController").GetComponent<NextGuiController>().OpenInventory();
    }

    public void CancelAtk()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().CancelAttack();
    }

    public void EndPlayerUnitTurn()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().EndPlayerUnitTurn();
    }
}
