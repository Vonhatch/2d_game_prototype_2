﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AttackConfirm : MonoBehaviour {
    public UnityEvent atk1Pressed;

	public void Atk1()
        {
        //atk1Pressed.Invoke();
        //this is bad code and needs to be reworked at some point
        GameObject.Find("GUIController").GetComponent<NextGuiController>().Attack(0);
        }

    public void CancelAtk()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().CancelAttack();
    }

    public void OpenInventory()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().OpenInventory();
    }

    public void CloseInventory()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().CloseInventory();
    }

    public void EndPlayerUnitTurn()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().EndPlayerUnitTurn();
    }
}
