﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
    public int maxCharges;
    public int currentCharges;

    public string itemName;
    public string description;

    public bool isEquipable;
    public bool isEquiped;

    public int equipMaxHitPoints = 0;


    public int equipAttackFactor = 0;
    public int equipDefenceFactor = 0;
    public int equipSkillFactor = 0;
    public int equipSpeedFactor = 0;
    public int equipTechnicalFactor = 0;
    public int equipTechDefenceFactor = 0;
    public int equipLuckFactor = 0;

}
