﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AttackBtn : MonoBehaviour, IPointerEnterHandler
{
    public int btnNo = 0;

    public UnityEvent atk1Pressed;

    public void OnPointerEnter(PointerEventData eventData)
    {
        // Do something.
        //Debug.Log("<color=red>Event:</color> Completed mouse highlight.");
        GameObject.Find("GUIController").GetComponent<NextGuiController>().ItemHighlighted(btnNo);
        //should prolly be handled by itemhighlighted eventually
        GameObject.Find("GUIController").GetComponent<NextGuiController>().ShowItemPanel(btnNo);
    }

    public void OnPointerExit(PointerEventData eventData)
    {

    }

    public void Atk1()
    {
        //atk1Pressed.Invoke();
        //this is bad code and needs to be reworked at some point
        GameObject.Find("GUIController").GetComponent<NextGuiController>().Attack(btnNo);
    }

    public void CancelAtk()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().CancelAttack();
    }

    public void EndPlayerUnitTurn()
    {
        GameObject.Find("GUIController").GetComponent<NextGuiController>().EndPlayerUnitTurn();
    }
}
