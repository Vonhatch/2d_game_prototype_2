﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Simple implementation of AI for the game.
/// </summary>
public class NaiveAiPlayer : Player
{
    //DEBUG!!! Do this differently later!!!!
    public NextGuiController guiCon;
    //END DEBUG

    //have this variable inherit from a global object with player selections (animations, animation speed, sound etc)
    //need another one of these wherever unit movement speed is handled
    //actually this would work a lot better if it just phoned Cell grid to find out the state of animations and such
    private float waitDuration = 4f;

    private CellGrid _cellGrid;
    private System.Random _rnd;

    /*void Start()
    {
        guiCon = GameObject.Find("GUIController");
    }*/

    public NaiveAiPlayer()
    {
        _rnd = new System.Random();
    }

    public override void Play(CellGrid cellGrid)
    {
        cellGrid.CellGridState = new CellGridStateAiTurn(cellGrid);
        _cellGrid = cellGrid;

        StartCoroutine(Play());

        //Coroutine is necessary to allow Unity to run updates on other objects (like UI).
        //Implementing this with threads would require a lot of modifications in other classes, as Unity API is not thread safe.
    }
    private IEnumerator Play()
    {
        //Debug.Log("runnuing play in naive ai player");
        //yield return new WaitForSeconds(1000);
        var myUnits = _cellGrid.Units.FindAll(u => u.PlayerNumber.Equals(PlayerNumber)).ToList();
        foreach (var unit in myUnits.OrderByDescending(u => u.Cell.GetNeighbours(_cellGrid.Cells).FindAll(u.IsCellTraversable).Count))
        {
            //print("An enemy has been selected");
            //DEBUG CODE, this prevents the enemy player from making a move for each new unit until the player presses the fire button
            while ( guiCon.levelPanelDisplaying )
            {
                yield return null;
            }
            //END DEBUG

            var enemyUnits = _cellGrid.Units.Except(myUnits).ToList();
            var unitsInRange = new List<Unit>();
            foreach (var enemyUnit in enemyUnits)
            {
                if (unit.IsUnitAttackable(enemyUnit, unit.Cell))
                {
                    unitsInRange.Add(enemyUnit);
                }
            }//Looking for enemies that are in attack range.
            if (unitsInRange.Count != 0)
            {
                var index = _rnd.Next(0, unitsInRange.Count);
                //change from base code
                ///unit.DealDamage(unitsInRange[index]);
                unit.Attack(unitsInRange[index]);
                //this determines how long the enemy AI is going to wait before moving to the next task
                //this is also done at the bottom of this class, for now just handle this manually to ensure attacks aren't way out of order
                //yield return new WaitForSeconds((float)waitDuration);

                while (unit != null && unit.isAttacking)
                {
                    yield return null;
                }

                continue;
            }//If there is an enemy in range, attack it.

            List<Cell> potentialDestinations = new List<Cell>();

            foreach (var enemyUnit in enemyUnits)
            {
                potentialDestinations.AddRange(_cellGrid.Cells.FindAll(c => unit.IsCellMovableTo(c) && unit.IsUnitAttackable(enemyUnit, c)));
            }//Making a list of cells that the unit can attack from.

            var notInRange = potentialDestinations.FindAll(c => c.GetDistance(unit.Cell) > unit.MovementPoints);
            potentialDestinations = potentialDestinations.Except(notInRange).ToList();

            if (potentialDestinations.Count == 0 && notInRange.Count != 0)
            {
                potentialDestinations.Add(notInRange.ElementAt(_rnd.Next(0, notInRange.Count - 1)));
            }

            potentialDestinations = potentialDestinations.OrderBy(h => _rnd.Next()).ToList();
            List<Cell> shortestPath = null;
            foreach (var potentialDestination in potentialDestinations)
            {
                var path = unit.FindPath(_cellGrid.Cells, potentialDestination);
                if ((shortestPath == null && path.Sum(h => h.MovementCost) > 0) || shortestPath != null && (path.Sum(h => h.MovementCost) < shortestPath.Sum(h => h.MovementCost) && path.Sum(h => h.MovementCost) > 0))
                    shortestPath = path;

                var pathCost = path.Sum(h => h.MovementCost);
                if (pathCost > 0 && pathCost <= unit.MovementPoints)
                {
                    unit.Move(potentialDestination, path);
                    while (unit.isMoving)
                        yield return 0;
                    shortestPath = null;
                    break;
                }
                //_cellGrid.ShowEnemyRange();

                yield return 0;
            }//If there is a path to any cell that the unit can attack from, move there.

            if (shortestPath != null)
            {
                foreach (var potentialDestination in shortestPath.Intersect(unit.GetAvailableDestinations(_cellGrid.Cells)).OrderByDescending(h => h.GetDistance(unit.Cell)))
                {
                    var path = unit.FindPath(_cellGrid.Cells, potentialDestination);
                    var pathCost = path.Sum(h => h.MovementCost);
                    if (pathCost > 0 && pathCost <= unit.MovementPoints)
                    {
                        unit.Move(potentialDestination, path);
                        while (unit.isMoving)
                            yield return 0;
                        break;
                    }

                    //_cellGrid.ShowEnemyRange();

                    yield return 0;
                }
            }//If the path cost is greater than unit movement points, move as far as possible.

            foreach (var enemyUnit in enemyUnits)
            {
                var enemyCell = enemyUnit.Cell;
                if (unit.IsUnitAttackable(enemyUnit, unit.Cell))
                {
                    unit.Attack(enemyUnit);
                    //yield return new WaitForSeconds((float)waitDuration);

                    while (unit != null && unit.isAttacking)
                    {
                        yield return null;
                    }

                    break;
                }
            }//Look for enemies in range and attack.
            _cellGrid.ShowEnemyRange();
        }
        _cellGrid.EndTurn();
    }
}