using UnityEngine;


public class CellGridStateAiTurn : CellGridState
{
    public CellGridStateAiTurn(CellGrid cellGrid) : base(cellGrid)
    {      
    }

    public override void OnStateEnter()
    {
        //Debug.Log("In AI turn state");
        base.OnStateEnter();
        foreach (var cell in _cellGrid.Cells)
        {
            cell.UnMark();
        }
    }
}