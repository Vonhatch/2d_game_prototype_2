﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class CellGridState
{
    protected CellGrid _cellGrid;
    
    protected CellGridState(CellGrid cellGrid)
    {
        _cellGrid = cellGrid;
    }

    /// <summary>
    /// Method is called when a unit is clicked on.
    /// </summary>
    /// <param name="unit">Unit that was clicked.</param>
    public virtual void OnUnitClicked(Unit unit)
    {
    }

    /// <summary>
    /// Method is called when a unit is right clicked on.
    /// </summary>
    /// <param name="unit">Unit that was clicked.</param>
    public virtual void OnUnitRightClicked(Unit unit)
    {
    }

    /// <summary>
    /// Method is called when a player unit is right clicked on.
    /// </summary>
    /// <param name="unit">Unit that was clicked.</param>
    public virtual void OnPlayerUnitRightClicked(Unit unit)
    {
    }

    /// <summary>
    /// Method is called when mouse exits cell's collider.
    /// </summary>
    /// <param name="cell">Cell that was deselected.</param>
    public virtual void OnCellDeselected(Cell cell)
    {
        /*if(cell is MySquare)
        {
            MySquare square = (MySquare)cell;
            if (square.markedAsAttackable)
            {
                return;
            }
        }*/
        //cell.UnMark();
        cell.Dehilight();
        //Debug.Log("feck");
    }

    /// <summary>
    /// Method is called when mouse enters cell's collider.
    /// </summary>
    /// <param name="cell">Cell that was selected.</param>
    public virtual void OnCellSelected(Cell cell)
    {
        cell.MarkAsHighlighted();
    }

    /// <summary>
    /// Method is called when a cell is clicked.
    /// </summary>
    /// <param name="cell">Cell that was clicked.</param>
    public virtual void OnCellClicked(Cell cell)
    {

    }


    /*
        /// <summary>
        /// 
        /// </summary>
        /// 
        public virtual void ShowEnemyRange(Unit Enemyunit)
        {
            //Debug.Log("showenemyRange triggered");
            HashSet<Cell> _pathsInEnemyRange = new HashSet<Cell>();

            _pathsInEnemyRange = Enemyunit.GetAvailableDestinations(_cellGrid.Cells);
            var cellsNotInEnemyRange = _cellGrid.Cells.Except(_pathsInEnemyRange);

            foreach (var cell in cellsNotInEnemyRange)
            {
                //cell.UnMark();
            }
            foreach (var cell in _pathsInEnemyRange)
            {
                cell.MarkAsEnemyReachable();
            }
        }*/


        //this is being called far too frequently in the enemy's turn, it's very inefficient and many of it's calls may well be unneeded
        //BUT I'M BUSY SO IT STAYS LIKE THIS FOR A BIT!!!
    public virtual void ShowEnemyRange(List<Unit> EnemyUnits)
    {
        //Debug.Log("showenemyRange triggered");
        HashSet<Cell> _pathsInEnemyRange = new HashSet<Cell>();
        //Debug.Log(EnemyUnits.Count.ToString());
        //Debug.Log("Showenemyrange Ran");

        foreach (Unit enemyUnit in EnemyUnits)
        {
            //Debug.Log(enemyUnit.MovementPoints.ToString());
            //Debug.Log(enemyUnit.name.ToString());
            HashSet<Cell> _enemycells = enemyUnit.GetAvailableDestinationsAtMaxMove(_cellGrid.Cells);
            _enemycells.Add(enemyUnit.Cell);

            HashSet<Cell> k = new HashSet<Cell>();
            for (int i = 0; i < enemyUnit.AttackRange; i++)
            {
                foreach (Cell theBloodyCell in _enemycells)
                {
                    List<Cell> holder = theBloodyCell.GetNeighbours(_cellGrid.Cells);

                    k.UnionWith(holder);
                }
                _enemycells.UnionWith(k);
            }
           // _enemycells.UnionWith(k);

            _pathsInEnemyRange.UnionWith(_enemycells);

            //this is completely unworkable, far too laggy
            /*
            HashSet<Cell> _cellHolder = new HashSet<Cell>();     
            for (int i = 0; i < enemyUnit.AttackRange; i++)
            {
                //Debug.Log("enemyrangepart ran");
                //Debug.Log(enemyUnit.AttackRange.ToString());
                foreach (Cell theBloodyCell in _enemycells)
                {
                    //Debug.Log("weeeeeeeeee");
                    List<Cell> holder = new List<Cell>();
                    holder.Add(theBloodyCell);
                    List<Cell> holder2 = theBloodyCell.GetNeighbours(holder);
                    foreach (Cell k in holder2)
                    {
                        Debug.Log("woooooooooo");
                        _cellHolder.Add(k);
                    }
                }
                _enemycells.UnionWith(_cellHolder);
            }*/

            //Debug.Log( _enemycells.Count().ToString()) ;

            /*
            foreach (Cell theBloodyCell in _enemycells)
            {
                //hash sets apparently can't have duplicates so this check is unnessecary              
                if (true)
                {
                    _pathsInEnemyRange.Add(theBloodyCell);
                }
            }*/

            //Debug.Log(_pathsInEnemyRange.Count().ToString());
            //_pathsInEnemyRange.Add(enemyUnit.GetAvailableDestinations(_cellGrid.Cells));
        }
        
        var cellsNotInEnemyRange = _cellGrid.Cells.Except(_pathsInEnemyRange);

        foreach (var cell in cellsNotInEnemyRange)
        {
            cell.UnMarkAsEnemyReachable();
        }
        foreach (var cell in _pathsInEnemyRange)
        {
            cell.MarkAsEnemyReachable();
        }
    }

    //this is being called far too frequently in the enemy's turn, it's very inefficient and many of it's calls may well be unneeded
    //BUT I'M BUSY SO IT STAYS LIKE THIS FOR A BIT!!!
    public virtual void ShowPlayerAttckRange(HashSet<Cell> pathsInRange, int minRange, int maxRange)
    {
        //Debug.Log("showplayerAttackRange triggered");
        //HashSet<Cell> _pathsInEnemyRange = new HashSet<Cell>();
        //Debug.Log(EnemyUnits.Count.ToString());
        //Debug.Log("Showenemyrange Ran");


        foreach(Cell cell in pathsInRange)
        {
            foreach(Cell cell2 in _cellGrid.Cells)
            {
                int distance = cell.GetDistance(cell2);
                if(distance <= maxRange && distance >= minRange)
                {
                    cell2.MarkAsAttackable();
                    //cell2.MarkAsReachable();
                    //Debug.Log("showplayerAttackRange cell has been marked as attackable");
                }
            }
        }

            //Debug.Log(enemyUnit.MovementPoints.ToString());
            //Debug.Log(enemyUnit.name.ToString());
           /* HashSet<Cell> _enemycells = enemyUnit.GetAvailableDestinationsAtMaxMove(_cellGrid.Cells);
            _enemycells.Add(enemyUnit.Cell);

            HashSet<Cell> k = new HashSet<Cell>();
            for (int i = 0; i < enemyUnit.AttackRange; i++)
            {
                foreach (Cell theBloodyCell in _enemycells)
                {
                    List<Cell> holder = theBloodyCell.GetNeighbours(_cellGrid.Cells);

                    k.UnionWith(holder);
                }
                _enemycells.UnionWith(k);
            }
            // _enemycells.UnionWith(k);

            _pathsInEnemyRange.UnionWith(_enemycells);*/

        
/*
        var cellsNotInEnemyRange = _cellGrid.Cells.Except(_pathsInEnemyRange);

        foreach (var cell in cellsNotInEnemyRange)
        {
            cell.UnMarkAsEnemyReachable();
        }
        foreach (var cell in _pathsInEnemyRange)
        {
            cell.MarkAsEnemyReachable();
        }
        */
    }

    /// <summary>
    /// Method is called on transitioning into a state.
    /// </summary>
    public virtual void OnStateEnter()
    {
        ShowEnemyRange(_cellGrid.HighlightedEnemyUnits);
        if (_cellGrid.Units.Select(u => u.PlayerNumber).Distinct().ToList().Count == 1)
        {
            _cellGrid.CellGridState = new CellGridStateGameOver(_cellGrid);
        }
    }



    /// <summary>
    /// Method is called on transitioning out of a state.
    /// </summary>
    public virtual void OnStateExit()
    {
    }
}