﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;


class CellGridStateUnitSelected : CellGridState
{

    private Unit _unit;
    private HashSet<Cell> _pathsInRange;
    private List<Unit> _unitsInRange;

    private Cell _unitCell;

    private List<Cell> _currentPath;

    private bool firstGrid = true;

    public CellGridStateUnitSelected(CellGrid cellGrid, Unit unit) : base(cellGrid)
    {
        _unit = unit;
        _pathsInRange = new HashSet<Cell>();
        _currentPath = new List<Cell>();
        _unitsInRange = new List<Unit>();
    }

    public CellGridStateUnitSelected(CellGrid cellGrid, Unit unit, bool first) : base(cellGrid)
    {
        if (!first)
        {
            firstGrid = false;
        }
        _unit = unit;
        _pathsInRange = new HashSet<Cell>();
        _currentPath = new List<Cell>();
        _unitsInRange = new List<Unit>();
    }

    public override void OnCellClicked(Cell cell)
    {
        if (_unit.isAttacking)
        {
            return;
        }
        
       if (_unit.isMoving)
            return;
       if (cell.IsTaken || !_pathsInRange.Contains(cell))
            {
                _cellGrid.CellGridState = new CellGridStateWaitingForInput(_cellGrid);
                return;
            }

            var path = _unit.FindPath(_cellGrid.Cells, cell);
            _unit.Move(cell, path);
        
            //offending code, causes calls to unit selected that shouldn't be made, however
            //I currently have no cases of this causing unintended behaviour as a result yet
            _cellGrid.CellGridState = new CellGridStateUnitSelected(_cellGrid, _unit);
        
    }
    public override void OnUnitClicked(Unit unit)
    {
        //debug, if errors occur, revert this to itself
        /*
        if (unit.Equals(_unit) || _unit.isMoving)
            return;
            */

        if (unit.Equals(_unit) && unit.isMenuing)
            _cellGrid.CellGridState = new CellGridStateUnitSelected(_cellGrid, _unit, false);

        if (_unit.isMoving || unit.Equals(_unit))
            return;



        if (_unitsInRange.Contains(unit) && _unit.ActionPoints > 0)
        {
            //_unit.DealDamage(unit);
            
            //_unit.Attack(unit);
            _unit.OnUnitChosenForAttack(_unit,unit);
            // print("attacking");

            //offending code, causes calls to unit selected that shouldn't be made, modified
            //to construct a new cell grid that doesn't call unit selected
            _cellGrid.CellGridState = new CellGridStateUnitSelected(_cellGrid, _unit, false);
        }

        if (unit.PlayerNumber.Equals(_unit.PlayerNumber))
        {
            if (_unit.isAttacking)
            {
                return;
            }
            
                _cellGrid.CellGridState = new CellGridStateUnitSelected(_cellGrid, unit);
            
        }
            
    }

    //important code related to reseting a unit when 
    public override void OnUnitRightClicked(Unit unit)
    {     
        if(unit.ActionPoints < 1)
        {
            return;
        }
        if(unit.UnitState is UnitStateMarkedAsSelected)
        {
            unit.ResetPosition();
            _cellGrid.CellGridState = new CellGridStateUnitSelected(_cellGrid, _unit);
        }

    }

    //important code related to reseting a unit when 
    public override void OnPlayerUnitRightClicked(Unit unit)
    {
        if (unit.ActionPoints < 1)
        {
            return;
        }
        if (unit.UnitState is UnitStateMarkedAsSelected)
        {
            unit.ResetPosition();
            _cellGrid.CellGridState = new CellGridStateUnitSelected(_cellGrid, _unit);
        }

    }

    public override void OnCellDeselected(Cell cell)
    {
        base.OnCellDeselected(cell);
        foreach(var _cell in _currentPath)
        {
            if (_pathsInRange.Contains(_cell))
                _cell.MarkAsReachable();
            else
                _cell.Dehilight();
        }
    }
    public override void OnCellSelected(Cell cell)
    {
        base.OnCellSelected(cell);
        if (!_pathsInRange.Contains(cell)) return;

        _currentPath = _unit.FindPath(_cellGrid.Cells, cell);
        foreach (var _cell in _currentPath)
        {
            _cell.MarkAsPath();
        }
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();

        if (firstGrid == true)
        {
            _unit.OnUnitSelected();
        }
        _unitCell = _unit.Cell;

        _pathsInRange = _unit.GetAvailableDestinations(_cellGrid.Cells);
        var cellsNotInRange = _cellGrid.Cells.Except(_pathsInRange);

        foreach (var cell in cellsNotInRange)
        {
            cell.UnMark();
        }
        foreach (var cell in _pathsInRange)
        {
            cell.MarkAsReachable();
        }
        /*
        foreach (var cell in cellsNotInRange)
        {
            if(_unit.Cell)
        }*/
        //ShowPlayerAttackRange(_pathsInRange, _unit.AttackRangeMin, _unit.AttackRange);
        ShowPlayerAttckRange(_pathsInRange, _unit.AttackRangeMin, _unit.AttackRange);

        if (_unit.ActionPoints <= 0) return;

        foreach (var currentUnit in _cellGrid.Units)
        {
            if (currentUnit.PlayerNumber.Equals(_unit.PlayerNumber))
                continue;
        
            if (_unit.IsUnitAttackable(currentUnit,_unit.Cell))
            {
                currentUnit.SetState(new UnitStateMarkedAsReachableEnemy(currentUnit));
                _unitsInRange.Add(currentUnit);
            }
        }

        if (_unitCell.GetNeighbours(_cellGrid.Cells).FindAll(c => c.MovementCost <= _unit.MovementPoints).Count == 0
            && _unitsInRange.Count == 0)
        {
            //with this code enabled units that have moved full distance and not attacked are marked as finished
            //_unit.SetState(new UnitStateMarkedAsFinished(_unit));
        }

        ShowEnemyRange(_cellGrid.HighlightedEnemyUnits);
    }
    public override void OnStateExit()
    {
        
        //ensures a unit isn't deselected when they select an enemy unit to attack and enter the attack confirmation menu
        if (/*!_unit.isAttacking &&*/ !_unit.isMenuing)
        {
            _unit.OnUnitDeselected();
        }
            
            foreach (var unit in _unitsInRange)
            {
                if (unit == null) continue;
                unit.SetState(new UnitStateNormal(unit));
            }
            foreach (var cell in _cellGrid.Cells)
            {
                cell.UnMark();
            }
    }
}


