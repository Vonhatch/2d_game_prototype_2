﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
//using System.Math;

using UnityEngine.EventSystems;

/// <summary>
/// Base class for all units in the game.
/// </summary>
public abstract class Unit : MonoBehaviour
{
    Dictionary<Cell, List<Cell>> cachedPaths = null;
    /// <summary>
    /// UnitClicked event is invoked when user clicks the unit. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitClicked;
    /// <summary>
    /// UnitRightClicked event is invoked when user clicks the unit. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitRightClicked;
    /// <summary>
    /// UnitSelected event is invoked when user clicks on unit that belongs to him. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitSelected;
    /// <summary>
    /// UnitDeselected event is invoked when user click outside of currently selected unit's collider. this is pretty inadequate for qualifying as deselected though
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitDeselected;
    /// <summary>
    /// UnitHighlighted event is invoked when user moves cursor over the unit. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitHighlighted;
    /// <summary>
    /// EndUnitTurnEnded event is invoked when a unit is marked as having ended its turn 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler EndUnitTurnEnded;
    /// <summary>
    /// EndPlayerTurnEnded event is invoked when the player unit is marked as having ended its turn 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler EndPlayerTurnEnded;
    /// <summary>
    /// UnitDehighlighted event is invoked when cursor exits unit's collider. 
    /// It requires a collider on the unit game object to work. DOES IT?!
    /// </summary>
    public event EventHandler UnitDehighlighted;
    /// <summary>
    /// UnitUsedConsumable is called after a unit has used a consumable
    /// It requires a collider on the unit game object to work. DOES IT?!
    /// </summary>
    public event EventHandler UnitUsedConsumable;
    /// <summary>
    /// UnitAttacked event is invoked when the unit is attacked.
    /// </summary>
    public event EventHandler<AttackEventArgs> UnitAttacked;
    /// <summary>
    /// UnitDestroyed event is invoked when unit's hitpoints drop below 0.
    /// </summary>
    public event EventHandler<AttackEventArgs> UnitDestroyed;
    /// <summary>
    /// UnitMoved event is invoked when unit moves from one cell to another.
    /// </summary>
    public event EventHandler<MovementEventArgs> UnitMoved;
    /// <summary>
    /// UnitChosenForAttack event is invoked when unit selects a target to attack
    /// </summary>
    public event EventHandler<BattleEventArgs> UnitChosenForAttack;

    public event EventHandler<LevelEventArgs> UnitLevelled;

    public UnitState UnitState { get; set; }
    public void SetState(UnitState state)
    {
        UnitState.MakeTransition(state);
    }

    /// <summary>
    /// A list of buffs that are applied to the unit.
    /// </summary>
    public List<Buff> Buffs { get; private set; }

    public int TotalHitPoints { get; private set; }
    public int TotalMovementPoints;
    protected int TotalActionPoints;

    /// <summary>
    /// Cell that the unit is currently occupying.
    /// </summary>
    public Cell Cell { get; set; }
    public Cell TurnStartCell { get; set; }

    public List<Item> Inventory = new List<Item>();
    //public Item[] Inventory = new Item[5];

    public int level = 1;
    public int experiencePoints = 0;

    public int HitPoints;

    public int totalAttackValue;
    public int hitMod = 0;
    public int critMod = 0;

    public int AttackFactor;
    public int DefenceFactor;
    public int SkillFactor;
    public int SpeedFactor;
    public int TechnicalFactor;
    public int TechDefenceFactor;
    public int LuckFactor;

    public int ModdedAttackFactor;
    public int ModdedDefenceFactor;
    public int ModdedSkillFactor;
    public int ModdedSpeedFactor;
    public int ModdedTechnicalFactor;
    public int ModdedTechDefenceFactor;
    public int ModdedLuckFactor;

    public int AttackType = 0; //being lazy here again, 0 is physical, 1 is technical


    /// <summary>
    /// Determines how far on the grid the unit can move.
    /// </summary>
    public int MovementPoints;
    public int AttackRange;
    public int AttackRangeMin = 1;

    public bool isAttacking = false;
    public bool isMenuing = false;

    public List<fightResult> fightResults = new List<fightResult>();
    public List<fightResult> fightResults2 = new List<fightResult>();
    //public List<BattleRound> fightResults = new List<BattleRound>();

    /// <summary>
    /// Determines if a unit should be reset when it is deselected, ie when attacking or otherwise ending its turn
    /// </summary>
    public bool turnEnded;


    /// <summary>
    /// Determines speed of movement animation.
    /// </summary>
    public float MovementSpeed;
    /// <summary>
    /// Determines how many attacks unit can perform in one turn.
    /// </summary>
    public int ActionPoints;

    /// <summary>
    /// Indicates the player that the unit belongs to. 
    /// Should correspoond with PlayerNumber variable on Player script.
    /// </summary>
    public int PlayerNumber;

    /// <summary>
    /// Indicates if movement animation is playing.
    /// </summary>
    public bool isMoving { get; set; }

    private static DijkstraPathfinding _pathfinder = new DijkstraPathfinding();
    private static IPathfinding _fallbackPathfinder = new AStarPathfinding();

    public bool markedAsHighlightedEnemy = false;

    private bool mouseUpped = true;
    /// <summary>
    /// Method called after object instantiation to initialize fields etc. 
    /// </summary>
    public virtual void Initialize()
    {
        //print(((int)1/(int)2).ToString());

        Buffs = new List<Buff>();

        UnitState = new UnitStateNormal(this);

        TotalHitPoints = HitPoints;
        TotalMovementPoints = MovementPoints;
        TotalActionPoints = ActionPoints;

        //this could cause problems
        //Debug.Log("enemy's are not starting with their equipped items equipped");
        EquipChanged();

        TurnStartCell = Cell;
    }

    protected virtual void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (UnitClicked != null)
                UnitClicked.Invoke(this, new EventArgs());
        }
    }

    protected virtual void Update()
    {
        if (Input.GetMouseButtonUp(1))
        {
            mouseUpped = true;
        }
        if (Input.GetMouseButtonDown(1))
        {
            mouseUpped = false;
        }
    }
    
    //this is a strange behavious where sometimes it wont pick up it's being moused over, this is inconsistent and is fixed after selecting a different unit?
    //inconsistent on orthographic, works fine on perspective, I'll just use perspective for now. this appears to still crop up even on perspective.
    protected virtual void OnMouseOver()
    {
        //Debug.Log("you've moused over me");
        if (UnitRightClicked != null && mouseUpped && Input.GetMouseButton(1) && !EventSystem.current.IsPointerOverGameObject())
        {
            if(isMenuing || isMoving)
            {
                return;
            }
            UnitRightClicked.Invoke(this, new EventArgs());
        }
    }

    protected virtual void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

            if (UnitHighlighted != null)
            UnitHighlighted.Invoke(this, new EventArgs());
    } 
    protected virtual void OnMouseExit()
    {
        if (UnitDehighlighted != null)
            UnitDehighlighted.Invoke(this, new EventArgs());
    }

    /// <summary>
    /// Method is called at the start of each turn.
    /// </summary>
    public virtual void OnTurnStart()
    {
        MovementPoints = TotalMovementPoints;
        ActionPoints = TotalActionPoints;

        SetState(new UnitStateMarkedAsFriendly(this));
    }
    /// <summary>
    /// Method is called at the end of each turn.
    /// </summary>
    public virtual void OnTurnEnd()
    {
        cachedPaths = null;
        Buffs.FindAll(b => b.Duration == 0).ForEach(b => { b.Undo(this); });
        Buffs.RemoveAll(b => b.Duration == 0);
        Buffs.ForEach(b => { b.Duration--; });

        TurnStartCell = Cell;

        SetState(new UnitStateNormal(this));
    }
    /// <summary>
    /// Method is called when units HP drops below 1.
    /// </summary>
    protected virtual void OnDestroyed()
    {
        Cell.IsTaken = false;
        MarkAsDestroyed();
        Destroy(gameObject);
    }

    /// <summary>
    /// Method is called when unit is selected.
    /// </summary>
    public virtual void OnUnitSelected()
    {
        //LevelUp();

        SetState(new UnitStateMarkedAsSelected(this));
        if (UnitSelected != null)
            UnitSelected.Invoke(this, new EventArgs());
    }

    public virtual void OnUnitChosenForAttack(Unit Attacker, Unit Defender)
    {
        UnitChosenForAttack.Invoke(this, new BattleEventArgs(Attacker, Defender));
    }
    /// <summary>
    /// Method is called when unit is deselected.
    /// </summary>
    public virtual void OnUnitDeselected()
    {
        //we will likely have to modify this code when it comes time to implement UI Unit commands like inventory or end
        //to prevent the game from thinking a unit has been deselected when navigating its inventory, also I'm almost certain
        //when you click and it asks for attack confirmation it will also run, perhaps set isAttacking to true during confirmation
        //check and then set it to false if they select no but ensure they're not declared as unselected.

        //also this is a mess, fix it later
        if (isAttacking == true || isMenuing)
        {

        }
        else if (UnitState is UnitStateMarkedAsFinished)
        {

        }
        else if(EventSystem.current.IsPointerOverGameObject())
        {

            return;
        }
        else
        {
            ResetPosition();
            SetState(new UnitStateMarkedAsFriendly(this));
            if (UnitDeselected != null)
                UnitDeselected.Invoke(this, new EventArgs());
        }


    }

    /// <summary>
    /// Method indicates if it is possible to attack unit given as parameter, 
    /// from cell given as second parameter.
    /// </summary>
    public virtual bool IsUnitAttackable(Unit other, Cell sourceCell)
    {
        //debug, perhaps we should make it perform this check for every single weapon the unit has that it can use?
        if (sourceCell.GetDistance(other.Cell) <= AttackRange && sourceCell.GetDistance(other.Cell) >= AttackRangeMin)
            return true;

        return false;
    }
    /// <summary>
    /// Method indicates if it is possible to attack unit with a certain weapon given as parameter, 
    /// from cell given as second parameter.
    /// </summary>
    public virtual bool IsUnitAttackable(Unit other, Cell sourceCell, Weapon weapon)
    {
        //debug, perhaps we should make it perform this check for every single weapon the unit has that it can use?
        if (sourceCell.GetDistance(other.Cell) <= weapon.attackRangeMax && sourceCell.GetDistance(other.Cell) >= weapon.attackRangeMin)
            return true;

        return false;
    }
    /// <summary>
    /// Attack
    /// </summary>
    ///
    //this will almost certainly fuck with the IENUMERATOR affecting animation, this will likely need to be overhauled to know the
    //outcomes of battle before the battle is starting and then do the animations to match the outcomes!!!
    public virtual void Attack(Unit other)
    {
        isAttacking = true;
        GetBattleResults(other);

        
        if (isMoving)
            return;
        if (ActionPoints == 0)
            return;
        if (!IsUnitAttackable(other, Cell))
            return;

        //this dictates whether a unit can still move, additionally the unitstatemarkedasfinished also changes the unit to a dark colour
        //this isn't necessarily good because they go dark before the turn is ended but if you click during the animation and you do this 
        //calc in ENDattack then you'll attack again because you still have attack points
        ActionPoints--;
        //here's an interesting, by default using all your actions stops movement, but this can be easily overcome.
        if (ActionPoints == 0)
        {
            MovementPoints = 0;
        }

        MarkAsAttacking(other);

    }

    public virtual void EndAttack(Unit other)
    {
        //this will cause nul refs until i have code to check if the unit is dead yet
        foreach (fightResult res in fightResults)
        {
            //print(res.ToString());
            if (this.HitPoints > 0 && other.HitPoints > 0)
            {
                if (res.attacker == 0)
                {
                    if (res.hit == 1)
                    {
                        other.Defend(this, res.damage);
                    }
                    WeaponUsed();
                }
                if (res.attacker == 1)
                {
                    if (res.hit == 1)
                    {
                        this.Defend(other, res.damage);
                    }
                    other.WeaponUsed();
                }
            }

            //if(hit)
        }
        fightResults.Clear();

        if (ActionPoints == 0)
        {
            SetState(new UnitStateMarkedAsFinished(this));

            if (UnitState is UnitStateMarkedAsFinished)
            {
            }
            MovementPoints = 0;

        }

        MarkAsAttacked(other);
        other.MarkAsAttacked(this);
        //this is in case I make units that can move after attacking, if they are then reset they will be reset to the position they attacked from not started from.
        //NOTE!!!! YOU WILL STILL NEED TO ADD CODE THAT TRACKS THE UNIT's REMAINING MOVEMENT POINTS FROM WHEN THEY ATTACKED TO NOT RESTORE THE MOVEMENT POINTS TO FULL!!!!
        TurnStartCell = Cell;

        GiveExp(BattleExperienceGain(other));
        other.GiveExp(BattleExperienceGain(this));

        isAttacking = false;

        if (EndUnitTurnEnded != null)
            EndUnitTurnEnded.Invoke(this, new EventArgs());
    }

    public void WeaponUsed()
    {
        int theonetodie = 9999;
        //Item Imdying = new Item();
        foreach (Item item in Inventory)
        {
            if (item is Weapon && item.isEquiped)
            {
                item.currentCharges--;
                if(item.currentCharges <= 0)
                {
                    Debug.Log("Need a weapon break function");
                    theonetodie = Inventory.IndexOf(item);
                    //Imdying = item;
                    //Inventory.Remove(item);
                    //Destroy(item);
                }
            }
        }
        if(theonetodie != 9999)
        {
            Destroy(Inventory[theonetodie].gameObject);
            Inventory.RemoveAt(theonetodie);
            //Destroy(item);
        }
    }

    //this will need code later
    public bool isWeaponEquippable(Weapon weapon)
    {

        return true;
    }

    public virtual int BattleExperienceGain(Unit other)
    {
        int exp = 0;

        if (other.HitPoints < 0)
        {
            //default is 30 for units of the same level
            exp = (int)Mathf.Clamp( (float)(30 + (5 * (other.level - this.level))), 0f, 100f);


        }
        else
        {
            //default is 10 for units of the same level
            exp = (int)Mathf.Clamp((float)(10 + (1 * (other.level - this.level))), 0f, 20f);

        }

        return exp;
    }

    public virtual void GiveExp(int exp)
    {
        //more hardcoded player number stuff
        if (PlayerNumber == 0)
        {
            this.experiencePoints += exp;
        }

        if (experiencePoints >= 100)
        {
            LevelUp();
        }
    }

    public virtual void LevelUp()
    {
        //Debug.Log("level Up");
        experiencePoints = this.experiencePoints - 100;

        LevelUpResult res = GetLevelUpStats();

        if (UnitLevelled != null)
            UnitLevelled.Invoke(this, new LevelEventArgs(res));

        this.level++;
        this.TotalHitPoints += res.HitPoints;
        this.AttackFactor += res.AttackFactor;
        this.DefenceFactor += res.DefenceFactor;
        this.SkillFactor += res.SkillFactor;
        this.SpeedFactor += res.SpeedFactor;
        this.TechnicalFactor += res.TechnicalFactor;
        this.TechDefenceFactor += res.TechDefenceFactor;
        this.LuckFactor += res.LuckFactor;

        //print(res.ToString());
        //some code to show the level up screen
    }

    //we might have to do it this way for animation and not clogging up enemy turns
    public virtual void EndLevelUp()
    {

    }

    public virtual LevelUpResult GetLevelUpStats()
    {
        //List<int> stats = new List<int>();
        LevelUpOdds odds = new LevelUpOdds();

        LevelUpResult res = new LevelUpResult();

        if (UnityEngine.Random.value < odds.HitPoints)
        {
            res.HitPoints++;
        }
        if (UnityEngine.Random.value < odds.AttackFactor)
        {
            res.AttackFactor++;
        }
        if (UnityEngine.Random.value < odds.DefenceFactor)
        {
            res.DefenceFactor++;
        }
        if (UnityEngine.Random.value < odds.SkillFactor)
        {
            res.SkillFactor++;
        }
        if (UnityEngine.Random.value < odds.SpeedFactor)
        {
            res.SpeedFactor++;
        }
        if (UnityEngine.Random.value < odds.TechnicalFactor)
        {
            res.TechnicalFactor++;
        }
        if (UnityEngine.Random.value < odds.TechDefenceFactor)
        {
            res.TechDefenceFactor++;
        }
        if (UnityEngine.Random.value < odds.LuckFactor)
        {
            res.LuckFactor++;
        }
        //print(res.ToString());
        return res;
    }

    //this cuurently only works when the unit is told by the UI to end its turn, I may eventually want to fuse this
    //with the turn end code that runs on each unit at the end of each player turn... the handle different things though,
    //it may be most convenient to keep them seperate as buffs should tick over at the end of each player's turn for trackability etc!!!
    public virtual void EndPlayerUnitTurn()
    {


        //this is in case I make units that can move after attacking, if they are then reset they will be reset to the position they attacked from not started from.
        //NOTE!!!! YOU WILL STILL NEED TO ADD CODE THAT TRACKS THE UNIT's REMAINING MOVEMENT POINTS FROM WHEN THEY ATTACKED TO NOT RESTORE THE MOVEMENT POINTS TO FULL!!!!
        TurnStartCell = Cell;

        if (UnitDeselected != null)
            UnitDeselected.Invoke(this, new EventArgs());
        if (EndPlayerTurnEnded != null)
            EndPlayerTurnEnded.Invoke(this, new EventArgs());


        SetState(new UnitStateMarkedAsFinished(this));
        ActionPoints = 0;
        MovementPoints = 0;
        /*
        List<Cell> path = new List<Cell>(0);
        if (UnitMoved != null)
            UnitMoved.Invoke(this, new MovementEventArgs(Cell, Cell, path));
            */

        //if (EndPlayerTurnEnded != null)
        //EndPlayerTurnEnded.Invoke(this, new EventArgs());

    }

    public virtual void GetBattleResults(Unit other)
    {
        fightResults.Clear();
        fightResults.Add(GetDamage(this, other, 0));
        if (other.IsUnitAttackable(this, other.Cell ))
        {
            fightResults.Add(GetDamage(other, this, 1));

        }
        
        if (doubleAttack(other) )
        {
            fightResults.Add(GetDamage(this, other, 0));
        }
        if (other.IsUnitAttackable(this, other.Cell))
        {
            if (other.doubleAttack(this))
            {
                fightResults.Add(GetDamage(other, this, 1));
            }
        }
    }

    //this exists to prevent a collection modification during battle animations
    public virtual void GetInfoBattleResults(Unit other)
    {
        fightResults2.Clear();
        fightResults2.Add(GetDamage(this, other, 0));
        if (other.IsUnitAttackable(this, other.Cell))
        {
            fightResults2.Add(GetDamage(other, this, 1));
        }

        if (doubleAttack(other))
        {
            fightResults2.Add(GetDamage(this, other, 0));
        }
        if (other.IsUnitAttackable(this, other.Cell))
        {
            if (other.doubleAttack(this))
            {
                fightResults2.Add(GetDamage(other, this, 1));
            }
        }
    }

    /*
    //this gives inforbattle results for a currently highlighted weapon, must be kept identical otherwise to above thingo
    public virtual void GetInfoBattleResults(Unit other, int itemNo)
    {
        int originalWeaponNo = 9999;

        foreach (Item item in Inventory)
        {
            if (item is Weapon && item.isEquiped)
            {
                originalWeaponNo = Inventory.IndexOf(item);
                Debug.Log(originalWeaponNo.ToString());
            }
        }

        EquipItem(itemNo);
        //nonbasecode

        fightResults2.Clear();
        fightResults2.Add(GetDamage(this, other, 0));
        if (other.IsUnitAttackable(this, other.Cell))
        {
            fightResults2.Add(GetDamage(other, this, 1));
        }

        if (doubleAttack(other))
        {
            fightResults2.Add(GetDamage(this, other, 0));
        }
        if (other.IsUnitAttackable(this, other.Cell))
        {
            if (other.doubleAttack(this))
            {
                fightResults2.Add(GetDamage(other, this, 1));
            }
        }


        //end nonbasecode
        if (originalWeaponNo != 9999)
        {
            EquipItem(originalWeaponNo);
        }
    }*/

    public void ItemSelected(int itemNo)
    {
        //Debug.Log("item selected ran: " + itemNo.ToString());
        if (Inventory[itemNo].isEquipable)
        {
            //unequips item if already equipped
            if (Inventory[itemNo].isEquiped)
            {
                Debug.Log("uneequiped currently equiped weapon");
                Inventory[itemNo].isEquiped = false;
                EquipChanged();
                return;
            }
            EquipItem(itemNo);
        }
        else if (Inventory[itemNo] is ConsumableItem)
        {
            UseConsumable(itemNo);
        }

        EquipChanged();
    }

    public void UseConsumable(int itemNo)
    {
        if (UnitUsedConsumable != null)
            UnitUsedConsumable.Invoke(this, new EventArgs());
        isMenuing = true;
        MarkAsItemUsed(this, itemNo);



    }

    public void EndUseConsumable(int itemNo)
    {

        ConsumableItem item = (ConsumableItem)Inventory[itemNo];

        HitPoints = Mathf.Clamp(HitPoints + item.hitPointRestore, 0, TotalHitPoints);

        Inventory[itemNo].currentCharges--;
        if (Inventory[itemNo].currentCharges <= 0)
        {
            Destroy(Inventory[itemNo].gameObject);
            Inventory.RemoveAt(itemNo);
        }



        
        isMenuing = false;
        EndPlayerUnitTurn();
    }

    public void EquipItem(int itemNo)
    {

        if (Inventory[itemNo].isEquipable && Inventory[itemNo] is Weapon)
        {
            foreach (Item item in Inventory)
            {
                if (item is Weapon)
                {
                    item.isEquiped = false;

                }

            }
            //Debug.Log("equiped new weapon: Item number - " + itemNo.ToString());
            Inventory[itemNo].isEquiped = true;
            //debug, this is a little hacky, I may want to change how this is handled later.
            //debug this causes a glitch where attackable enemies are not immediately updated.
            Weapon wep = (Weapon)Inventory[itemNo];
            //AttackRange = wep.attackRangeMax;
            //AttackRangeMin = wep.attackRangeMin;

            //this doesn't fix the glitch
            SetState(new UnitStateMarkedAsSelected(this));
            if (UnitSelected != null)
                UnitSelected.Invoke(this, new EventArgs());
        }
        else if (Inventory[itemNo].isEquipable)
        {
            if (!Inventory[itemNo].isEquiped)
            {
                Inventory[itemNo].isEquiped = true;
                Debug.Log("non weapon equipped");
            }
            else
            {
                Inventory[itemNo].isEquiped = false;
                Debug.Log("non weapon unequipped");
            }
        }

        EquipChanged();
    }

    public void UnequipItem(int itemNo)
    {
        Inventory[itemNo].isEquiped = false;
        EquipChanged();
    }

    public virtual void EquipChanged()
    {
        AttackRange = 0;
        AttackRangeMin = 99;

        ModdedAttackFactor = AttackFactor;
        ModdedDefenceFactor = DefenceFactor;
        ModdedSkillFactor = SkillFactor;
        ModdedSpeedFactor = SpeedFactor;
        ModdedTechnicalFactor = TechnicalFactor;
        ModdedTechDefenceFactor = TechDefenceFactor;
        ModdedLuckFactor = LuckFactor;

        foreach (Item item in Inventory)
        {
            if (item.isEquiped)
            //code here to handle mods that change things other than weapon attack and hit and crit
            {
                ModdedAttackFactor += item.equipAttackFactor;
                ModdedDefenceFactor += item.equipDefenceFactor;
                ModdedSkillFactor += item.equipSkillFactor;
                ModdedSpeedFactor += item.equipSpeedFactor;
                ModdedTechnicalFactor += item.equipTechnicalFactor;
                ModdedTechDefenceFactor += item.equipTechDefenceFactor;
                ModdedLuckFactor += item.equipLuckFactor;
            }
        }

        totalAttackValue = ModdedAttackFactor;

        foreach (Item item in Inventory)
        {
            if (item.isEquiped && item is Weapon)
            {
                Weapon wep = (Weapon)item;

                if (wep.attackType == 0)
                {
                    AttackType = wep.attackType;
                    totalAttackValue = ModdedAttackFactor + wep.attackModifier;
                }
                else
                {
                    AttackType = wep.attackType;
                    totalAttackValue = ModdedTechnicalFactor + wep.attackModifier;
                }

                hitMod = wep.hitModifier;
                critMod = wep.critModifier;
            }
            //prolly swap this to use wep at some point
            if (item is Weapon)
            {
                Weapon weapon = (Weapon)item;
                if (isWeaponEquippable(weapon))
                {
                    if (weapon.attackRangeMin < AttackRangeMin)
                    {
                        AttackRangeMin = weapon.attackRangeMin;
                    }
                    if (weapon.attackRangeMax > AttackRange)
                    {
                        AttackRange = weapon.attackRangeMax;
                    }
                }
            }
        }
                //I'm thinking all changes to base stats are handled by simply giving the player a list of MODS, there are different classes of mods, IE BUFFS/DEBUFFS and WEAPON?GEAR mods
    }

    /// <summary>
    /// Method deals damage to unit given as parameter.
    /// </summary>
    ///
    public virtual fightResult GetDamage(Unit attacker, Unit target, int attackerID)
    {
        
        if (attacker.hitChance(target) >= UnityEngine.Random.Range(0.0f, 100f))
        {
            if (attacker.AttackType == 0)
            {
                fightResult result = new fightResult(attackerID, Mathf.Clamp(attacker.totalAttackValue - target.ModdedDefenceFactor, 0, 9999), 1);
                return result;
            }
            else
            {
                fightResult result = new fightResult(attackerID, Mathf.Clamp(attacker.totalAttackValue - target.ModdedTechDefenceFactor, 0, 9999), 1);
                return result;
            }                    
        }
        else
        {
            if (attacker.AttackType == 0)
            {
                fightResult result = new fightResult(attackerID, Mathf.Clamp(attacker.totalAttackValue - target.ModdedDefenceFactor, 0, 9999), 0);
                return result;
            }
            else
            {
                fightResult result = new fightResult(attackerID, Mathf.Clamp(attacker.totalAttackValue - target.ModdedTechDefenceFactor, 0, 9999), 0);
                return result;
            }
        }
    }

    public virtual bool doubleAttack(Unit target)
    {
        if(this.ModdedSpeedFactor >= target.ModdedSpeedFactor + 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual int hitChance(Unit target)
    {
        return ((this.ModdedSkillFactor * 2 + this.ModdedLuckFactor) + hitMod - (target.ModdedSpeedFactor * 2 + target.ModdedLuckFactor));
    }

    /// <summary>
    /// Attacking unit calls Defend method on defending unit. 
    /// </summary>
    protected virtual void Defend(Unit other, int damage)
    {
        //maybe on hit plays markasdefending and have another for markasdodging?
        //MarkAsDefending(other);
        //explain formulae
        
        if (true)
        {           
            HitPoints -= Mathf.Clamp(damage, 0, damage);
        }
        //print(  ((other.SkillFactor * 2 + other.LuckFactor / 2) + 80 - (SpeedFactor + LuckFactor)).ToString() );

        if (UnitAttacked != null)
            UnitAttacked.Invoke(this, new AttackEventArgs(other, this, damage));


        if (HitPoints <= 0)
        {
            //this is meant to be done after ondestroyed is called but because i check for the availability of the cell when the unit destroyed message is sent i need it done here
            Cell.IsTaken = false;
            //if there are any bugs then this may well be the cause of them!!!!!!!!!!!
            if (UnitDestroyed != null)
                UnitDestroyed.Invoke(this, new AttackEventArgs(other, this, damage));
            OnDestroyed();
        }
    }

    /// <summary>
    /// Returns unit to its starting location for this turn.
    /// </summary>
    public virtual void ResetPosition(/*Cell destinationCell, List<Cell> path*/)
    {
        if (isMoving)
            return;

        //isMenuing = false;

        MovementPoints = TotalMovementPoints;

        Cell.IsTaken = false;
        Cell.IsTakenType = 999;
        Cell = TurnStartCell;
        TurnStartCell.IsTaken = true;
        TurnStartCell.IsTakenType = PlayerNumber;

        transform.position = Cell.transform.position;





        //useful code for later?
        List<Cell> path = new List<Cell>(0);
        if (UnitMoved != null)
            UnitMoved.Invoke(this, new MovementEventArgs(Cell, Cell, path));
    }

    /// <summary>
    /// Moves the unit to destinationCell along the path.
    /// </summary>
    public virtual void Move(Cell destinationCell, List<Cell> path)
    {
        if (isMoving)
            return;

        var totalMovementCost = path.Sum(h => h.MovementCost);
        if (MovementPoints < totalMovementCost)
            return;

        MovementPoints -= totalMovementCost;

        Cell.IsTaken = false;
        Cell.IsTakenType = 999;
        Cell = destinationCell;
        destinationCell.IsTaken = true;
        destinationCell.IsTakenType = PlayerNumber;

        //once a unit has moved it can't move again without resetting to avoid awkward things
        //this doesn't stop a unit from not being reset though.
        MovementPoints = 0;

        if (MovementSpeed > 0)
            StartCoroutine(MovementAnimation(path));
        else
            transform.position = Cell.transform.position;

        if (UnitMoved != null)
            UnitMoved.Invoke(this, new MovementEventArgs(Cell, destinationCell, path));    



    }



    protected virtual IEnumerator MovementAnimation(List<Cell> path)
    {
        isMoving = true;
        path.Reverse();
        foreach (var cell in path)
        {
            Vector3 destination_pos = new Vector3(cell.transform.localPosition.x, transform.localPosition.y, cell.transform.localPosition.z);
            while (transform.localPosition != destination_pos)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, destination_pos, Time.deltaTime * MovementSpeed);
                yield return 0;
            }
        }
        isMoving = false;
    }

    ///<summary>
    /// Method indicates if unit is capable of moving to cell given as parameter.
    /// </summary>
    public virtual bool IsCellMovableTo(Cell cell)
    {
        return !cell.IsTaken;
    }
    /// <summary>
    /// Method indicates if unit is capable of moving through cell given as parameter.
    /// </summary>
    public virtual bool IsCellTraversable(Cell cell)
    {
        if(cell.IsTaken && cell.IsTakenType == PlayerNumber)
        {
            return true;
        }

        return !cell.IsTaken;
    }
    /// <summary>
    /// Method returns all cells that the unit is capable of moving to.
    /// </summary>
    public HashSet<Cell> GetAvailableDestinations(List<Cell> cells)
    {
        cachedPaths = new Dictionary<Cell, List<Cell>>();
        
        var paths = cachePaths(cells);
        foreach (var key in paths.Keys)
        {
            if (!IsCellMovableTo(key))
                continue;
            var path = paths[key];

            var pathCost = path.Sum(c => c.MovementCost);
            if (pathCost <= MovementPoints)
            {
                cachedPaths.Add(key, path);
            }
        }
        return new HashSet<Cell>(cachedPaths.Keys);
    }

    /// <summary>
    /// Method returns all cells that the unit is capable of moving to at max movement, used only for showing enemy range when they are right clicked
    /// This will eventually be called something like show max enemy range and will show everwyhere the enemy can attack by moving, not just where they can move to.
    /// </summary>
    public HashSet<Cell> GetAvailableDestinationsAtMaxMove(List<Cell> cells)
    {
        cachedPaths = new Dictionary<Cell, List<Cell>>();

        var paths = cachePaths(cells);
        foreach (var key in paths.Keys)
        {
            if (!IsCellMovableTo(key))
                continue;
            var path = paths[key];

            var pathCost = path.Sum(c => c.MovementCost);
            if (pathCost <= TotalMovementPoints)
            {
                cachedPaths.Add(key, path);
            }
        }
        return new HashSet<Cell>(cachedPaths.Keys);
    }

    private Dictionary<Cell, List<Cell>> cachePaths(List<Cell> cells)
    {
        var edges = GetGraphEdges(cells);
        var paths = _pathfinder.findAllPaths(edges, Cell);
        return paths;
    }

    public List<Cell> FindPath(List<Cell> cells, Cell destination)
    {
        if(cachedPaths != null && cachedPaths.ContainsKey(destination))
        {
            return cachedPaths[destination];
        }
        else
        {
            return _fallbackPathfinder.FindPath(GetGraphEdges(cells), Cell, destination);
        }
    }
    /// <summary>
    /// Method returns graph representation of cell grid for pathfinding.
    /// </summary>
    protected virtual Dictionary<Cell, Dictionary<Cell, int>> GetGraphEdges(List<Cell> cells)
    {
        Dictionary<Cell, Dictionary<Cell, int>> ret = new Dictionary<Cell, Dictionary<Cell, int>>();
        foreach (var cell in cells)
        {
            if (IsCellTraversable(cell) || cell.Equals(Cell))
            {
                ret[cell] = new Dictionary<Cell, int>();
                foreach (var neighbour in cell.GetNeighbours(cells).FindAll(IsCellTraversable))
                {
                    ret[cell][neighbour] = neighbour.MovementCost;
                }
            }
        }
        return ret;
    }

    public virtual void MarkAsItemUsed(Unit target, int itemNo)
    {

    }

    public void MarkAsHealing(Unit other)
    {

    }
    /// <summary>
    /// Gives visual indication that the unit is under attack.
    /// </summary>
    /// <param name="other"></param>
    public abstract void MarkAsDefending(Unit other);
    /// <summary>
    /// Gives visual indication that the unit is attacking.
    /// </summary>
    /// <param name="other"></param>
    public abstract void MarkAsAttacking(Unit other);
    public abstract void MarkAsAttacked(Unit other);
    /// <summary>
    /// Gives visual indication that the unit is destroyed. It gets called right before the unit game object is
    /// destroyed, so either instantiate some new object to indicate destruction or redesign Defend method. 
    /// </summary>
    public abstract void MarkAsDestroyed();

    /// <summary>
    /// Method marks unit as current players unit.
    /// </summary>
    public abstract void MarkAsFriendly();
    /// <summary>
    /// Method mark units to indicate user that the unit is in range and can be attacked.
    /// </summary>
    public abstract void MarkAsReachableEnemy();
    /// <summary>
    /// Method marks unit as currently selected, to distinguish it from other units.
    /// </summary>
    public abstract void MarkAsSelected();
    /// <summary>
    /// Method marks unit to indicate user that he can't do anything more with it this turn.
    /// </summary>
    public abstract void MarkAsFinished();
    /// <summary>
    /// Method marks unit to indicate user that he is one of the enemies showing their attack range.
    /// </summary>
    public abstract void MarkAsHighlightedEnemy();
    /// <summary>
    /// Method returns the unit to its base appearance
    /// </summary>
    public abstract void UnMark();
}

public class MovementEventArgs : EventArgs
{
    public Cell OriginCell;
    public Cell DestinationCell;
    public List<Cell> Path;

    public MovementEventArgs(Cell sourceCell, Cell destinationCell, List<Cell> path)
    {
        OriginCell = sourceCell;
        DestinationCell = destinationCell;
        Path = path;
    }
}
public class AttackEventArgs : EventArgs
{
    public Unit Attacker;
    public Unit Defender;

    public int Damage;

    public AttackEventArgs(Unit attacker, Unit defender, int damage)
    {
        Attacker = attacker;
        Defender = defender;

        Damage = damage;
    }
}
public class UnitCreatedEventArgs : EventArgs
{
    public Transform unit;

    public UnitCreatedEventArgs(Transform unit)
    {
        this.unit = unit;
    }
}
public class BattleEventArgs : EventArgs
{
    public Unit Attacker;
    public Unit Defender;

    public BattleEventArgs(Unit attacker, Unit defender)
    {
        Attacker = attacker;
        Defender = defender;
    }
}
public class LevelEventArgs : EventArgs
{
    public LevelUpResult result;

    public LevelEventArgs(LevelUpResult Result)
    {
        result = Result;
    }
}
