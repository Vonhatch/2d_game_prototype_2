﻿using UnityEngine;

class CellGridStateWaitingForInput : CellGridState
{
    public CellGridStateWaitingForInput(CellGrid cellGrid) : base(cellGrid)
    {
    }

    public override void OnUnitClicked(Unit unit)
    {
        //debug code used to find the cause of the select enemy during player turn glitch
        //_cellGrid.CurrentPlayerDebug();
        if (unit.PlayerNumber.Equals(_cellGrid.CurrentPlayerNumber))

            _cellGrid.CellGridState = new CellGridStateUnitSelected(_cellGrid, unit);
    }

    public override void OnStateEnter()
    {
        ShowEnemyRange(_cellGrid.HighlightedEnemyUnits);
        //Debug.Log("Entered waiting for input");
    }
}
