﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.EventSystems;

/// <summary>
/// CellGrid class keeps track of the game, stores cells, units and players objects. It starts the game and makes turn transitions. 
/// It reacts to user interacting with units or cells, and raises events related to game progress. 
/// </summary>
public class CellGrid : MonoBehaviour
{
    /// <summary>
    /// LevelLoading event is invoked before Initialize method is run.
    /// </summary>
    public event EventHandler LevelLoading;
    /// <summary>
    /// LevelLoadingDone event is invoked after Initialize method has finished running.
    /// </summary>
    public event EventHandler LevelLoadingDone;
    /// <summary>
    /// GameStarted event is invoked at the beggining of StartGame method.
    /// </summary>
    public event EventHandler GameStarted;
    /// <summary>
    /// GameEnded event is invoked when there is a single player left in the game.
    /// </summary>
    public event EventHandler GameEnded;
    /// <summary>
    /// Turn ended event is invoked at the end of each turn.
    /// </summary>
    public event EventHandler TurnEnded;

    /// <summary>
    /// UnitAdded event is invoked each time AddUnit method is called.
    /// </summary>
    public event EventHandler<UnitCreatedEventArgs> UnitAdded;
    
    private CellGridState _cellGridState; //The grid delegates some of its behaviours to cellGridState object.
    public CellGridState CellGridState
    {
        private get
        {
            return _cellGridState;
        }
        set
        {
            if(_cellGridState != null)
                _cellGridState.OnStateExit();
            _cellGridState = value;
            _cellGridState.OnStateEnter();
        }
    }

    public int NumberOfPlayers { get; private set; }

    public Player CurrentPlayer
    {
        get { return Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)); }
    }
    public int CurrentPlayerNumber { get; private set; }

    /// <summary>
    /// GameObject that holds player objects.
    /// </summary>
    public Transform PlayersParent;

    public List<Player> Players { get; private set; }
    public List<Cell> Cells { get; private set; }
    public List<Unit> Units { get; private set; }

    public List<Unit> HighlightedEnemyUnits { get; private set; }

    public int turnNo = 0;

    private void Start()
    {
        if (LevelLoading != null)
            LevelLoading.Invoke(this, new EventArgs());

        Initialize();

        if (LevelLoadingDone != null)
            LevelLoadingDone.Invoke(this, new EventArgs());

        StartGame();
    }

    public void CurrentPlayerDebug()
    {
        print("current player is " + CurrentPlayerNumber);
    }

    //DANGER POINT!//
    //all the init stuff handled here could be an issue if I add unit reinforcements
    private void Initialize()
    {
        Players = new List<Player>();
        for (int i = 0; i < PlayersParent.childCount; i++)
        {
            var player = PlayersParent.GetChild(i).GetComponent<Player>();
            if (player != null)
                Players.Add(player);
            else
                Debug.LogError("Invalid object in Players Parent game object");
        }
        NumberOfPlayers = Players.Count;
        CurrentPlayerNumber = Players.Min(p => p.PlayerNumber);

        Cells = new List<Cell>();
        for (int i = 0; i < transform.childCount; i++)
        {
            var cell = transform.GetChild(i).gameObject.GetComponent<Cell>();
            if (cell != null)
                Cells.Add(cell);
            else
                Debug.LogError("Invalid object in cells paretn game object");
        }

        foreach (var cell in Cells)
        {
            cell.CellClicked += OnCellClicked;
            cell.CellHighlighted += OnCellHighlighted;
            cell.CellDehighlighted += OnCellDehighlighted;
            cell.GetComponent<Cell>().GetNeighbours(Cells);
        }

        var unitGenerator = GetComponent<IUnitGenerator>();
        if (unitGenerator != null)
        {
            Units = unitGenerator.SpawnUnits(Cells);
            foreach (var unit in Units)
            {
                unit.UnitRightClicked += OnUnitRightClicked;
                AddUnit(unit.GetComponent<Transform>());
            }
        }
        else
            Debug.LogError("No IUnitGenerator script attached to cell grid");

        HighlightedEnemyUnits = new List<Unit>();
    }

    //this is needed to regenerate the movement grid when a unit is restored to its starting position
    private void OnUnitRightClicked(object sender, EventArgs e)
    {
        //this is the key
        //CellGridState.OnUnitRightClicked(sender as Unit);
        var unit = sender as GenericUnit;

        //more hardcoded player number shit
        if (CurrentPlayerNumber != 0)
        {
            return;
        }

        if (unit.PlayerNumber == 0)
        {
            CellGridState.OnPlayerUnitRightClicked(sender as Unit);
        }
        else
        {
            if (HighlightedEnemyUnits.Contains(unit))
            {
                HighlightedEnemyUnits.Remove(unit);
                if (unit.ActionPoints > 0)
                {
                    unit.markedAsHighlightedEnemy = false;
                    unit.UnMark();               
                }
                else
                {
                    unit.markedAsHighlightedEnemy = false;
                    unit.UnMark();
                }
            }
            else
            {
                HighlightedEnemyUnits.Add(unit);
                unit.MarkAsHighlightedEnemy();
            }
            ShowEnemyRange();
        }
    }

    public void ShowEnemyRange()
    {
        CellGridState.ShowEnemyRange(HighlightedEnemyUnits);
    }

    private void OnCellDehighlighted(object sender, EventArgs e)
    {
        //var sender as Cell cell;

        CellGridState.OnCellDeselected(sender as Cell);
    }
    private void OnCellHighlighted(object sender, EventArgs e)
    {

        CellGridState.OnCellSelected(sender as Cell);
        //CellGridState.ShowEnemyRange(HighlightedEnemyUnits);
    } 
    private void OnCellClicked(object sender, EventArgs e)
    {
        //the master controller for all cell clicked actions starts here I believe, ui overides should be done here!

        if (EventSystem.current.IsPointerOverGameObject())
        {
            //very useful code to find out what objects are being detected by the mouse
            /*
            PointerEventData pointer = new PointerEventData(EventSystem.current);
            pointer.position = Input.mousePosition;

            List<RaycastResult> raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointer, raycastResults);

            if (raycastResults.Count > 0)
            {
                foreach (var go in raycastResults)
                {
                    Debug.Log(go.gameObject.name, go.gameObject);
                }
            }*/


            return;
        }


        CellGridState.OnCellClicked(sender as Cell);
    }

    private void OnUnitClicked(object sender, EventArgs e)
    {
        //the master controller for all unit clicked actions that relate to cell grid states starts here I believe, ui overides should be done here!
        /*var unit = sender as GenericUnit;
        if (!unit.PlayerNumber.Equals(CurrentPlayerNumber))
            {
            print("no allowed to select unit");
        }*/

        CellGridState.OnUnitClicked(sender as Unit);
    }
    private void OnUnitDestroyed(object sender, AttackEventArgs e)
    {
        if (HighlightedEnemyUnits.Contains(sender as Unit))
        {
            //Debug.Log("A destroyed unit was removed from HighlightedEnemyUnits");
            HighlightedEnemyUnits.Remove(sender as Unit);
            //_cellGridState.ShowEnemyRange(HighlightedEnemyUnits);
        }

        Units.Remove(sender as Unit);
        //Debug.Log("A unit died");
        ShowEnemyRange();
        var totalPlayersAlive = Units.Select(u => u.PlayerNumber).Distinct().ToList(); //Checking if the game is over
        if (totalPlayersAlive.Count == 1)
        {
            if(GameEnded != null)
                GameEnded.Invoke(this, new EventArgs());
        }
    }

    private void OnEndPlayerTurnEnded(object sender, EventArgs e)
    {
        CellGridState = new CellGridStateWaitingForInput(this);
        /*
        //THIS IS DEBUG STUFF!!!! I shouldn't be hardcording this
        if (CurrentPlayerNumber == 0)
        {
            //CellGridState = new CellGridStateWaitingForInput(this);
        }*/
    }

    private void OnEndUnitTurnEnded(object sender, EventArgs e)
    {
        //DEBUG HARDCODED PLAYER VALUE CHECK, MAY WANNA CHECK LATER
        if (CurrentPlayerNumber == 0)
        {
            CellGridState = new CellGridStateWaitingForInput(this);
        }
    }

    private void OnUnitDeselected(object sender, EventArgs e)
    {
        //print("deselected");
    }

    //debug this solely currently exists to fix a bug where once a player's inventory changes the game needs to refresh what the player can now hit.
    //there is likely a much better fix to this
    private void OnUnitSelected(object sender, EventArgs e)
    {
        //print("deselected");
        CellGridState.OnUnitClicked(sender as Unit);
    }

    /// <summary>
    /// Adds unit to the game
    /// </summary>
    /// <param name="unit">Unit to add</param>
    public void AddUnit(Transform unit)
    {
        unit.GetComponent<Unit>().UnitClicked += OnUnitClicked;
        unit.GetComponent<Unit>().UnitDestroyed += OnUnitDestroyed;
        //unit.GetComponent<Unit>().UnitDeselected += OnUnitDeselected;
        unit.GetComponent<Unit>().UnitSelected += OnUnitSelected;
        unit.GetComponent<Unit>().EndPlayerTurnEnded += OnEndPlayerTurnEnded;
        unit.GetComponent<Unit>().EndUnitTurnEnded += OnEndUnitTurnEnded;

        if (UnitAdded != null)
            UnitAdded.Invoke(this, new UnitCreatedEventArgs(unit)); 
    }

    /// <summary>
    /// Method is called once, at the beggining of the game.
    /// </summary>
    public void StartGame()
    {
        if(GameStarted != null)
            GameStarted.Invoke(this, new EventArgs());

        Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnStart(); });
        Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)).Play(this);
    }
    /// <summary>
    /// Method makes turn transitions. It is called by player at the end of his turn.
    /// </summary>
    /// //currently a unit's position will be reset when end turn is clicked without ending the unit's turn first
    /// this probably has something to do with onunitdeselected bein called
    public void EndTurn()
    {
        foreach (Unit unit in Units)
        {
            if (unit.isMoving || unit.isAttacking || unit.isMenuing)
            {
                Debug.Log("A unit is still in the moving, attacking menuing phase and that could be an issue");
                //return;
            }
            //another example of a hardcoded player number eference
            if ((unit.isMoving || unit.isAttacking || unit.isMenuing) && CurrentPlayerNumber == 0)
            {
                Debug.Log("A player unit is still attacking, menuing or moving end turn will not be allowed");
                return;
            }
        }

        Debug.Log("End of Turn: " + turnNo.ToString());
        turnNo++;

        if (Units.Select(u => u.PlayerNumber).Distinct().Count() == 1)
        {
            return;
        }
        CellGridState = new CellGridStateTurnChanging(this);

        Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnEnd(); });

        CurrentPlayerNumber = (CurrentPlayerNumber + 1) % NumberOfPlayers;
        while (Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).Count == 0)
        {
            CurrentPlayerNumber = (CurrentPlayerNumber + 1)%NumberOfPlayers;
        }//Skipping players that are defeated.

        if (TurnEnded != null)
            TurnEnded.Invoke(this, new EventArgs());

        Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnStart(); });
        Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)).Play(this);

        //Debug.Log("made it to the end of the endturn code, now is turn: " + turnNo.ToString());
        ShowEnemyRange();
    }
}
